<?php


use Illuminate\Support\Facades\Route;


// // Route::group([

// //     'middleware' => 'api',
// //     'prefix' => 'auth'

// ], function ($router) {

//     Route::post('login', 'AuthController@login');
//     Route::post('logout', 'AuthController@logout');
//     Route::post('refresh', 'AuthController@refresh');
//     Route::post('me', 'AuthController@me');

// });


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
    //return $request->user();
//});

//--MGPAIS--
Route::post('cargar_pais','paisController@cargarPais');
Route::put('listar_pais','paisController@listarPais');
Route::post('modificar_pais','paisController@modificarPais');
Route::post('insertar_pais','paisController@insertarPais');


//--MGTIPOPERSONA--
Route::put('cargar_tipopersona','tipopersonaController@cargarTipopersona');
Route::post('insertar_tipopersona','tipopersonaController@insertarTipopersona');
Route::put('listar_tipopersona','tipopersonaController@listarTipopersona');
Route::post('modificar_tipopersona','tipopersonaController@modificarTipopersona');

//-- MGSEXO--
Route::put('cargar_sexo','sexoController@cargarSexo');
Route::put('listar_sexo','sexoController@listarSexo');
Route::post('modificar_sexo','sexoController@modificarSexo');
Route::post('insertar_sexo','sexoController@insertarSexo');

//---MGPERSONA---
Route::put('cargar_persona','personaController@cargarPersona');
Route::put('listar_persona','personaController@listarPersona');
Route::post('modificar_persona','personaController@modificarPersona');
Route::post('insertar_persona','personaController@insertarPersona');

//---MGUBIGEO---
Route::put('cargar_ubigeo','ubigeoController@cargarUbigeo');
Route::put('listar_ubigeo','ubigeoController@listarUbigeo');
Route::post('modificar_ubigeo','ubigeoController@modificarUbigeo');
Route::post('insertar_ubigeo','ubigeoController@insertarUbigeo');


//---SEUSUARIO---
Route::put('cargar_usuario','usuarioController@cargarUsuario');
Route::put('listar_usuario','usuarioController@listarUsuario');
Route::post('modificar_usuario','usuarioController@modificarUsuario');
Route::post('insertar_usuario','usuarioController@insertarUsuario');
Route::put('inicio_sesion','usuarioController@inicioSesion');
Route::put('recursividad_ubigeo','CargarDatosController@cargarArbol');

//---SEFUNCION---
Route::put('cargar_funcion','funcionController@cargarFuncion');
Route::put('listar_funcion','funcionController@listarFuncion');
Route::post('modificar_funcion','funcionController@modificarFuncion');
Route::post('insertar_funcion','funcionController@insertarFuncion');

//---MGALMACENTIPO--
Route::put('cargar_almacentipo','almacentipoController@cargarAlmacentipo');
Route::put('listar_almacentipo','almacentipoController@listarAlmacentipo');
Route::post('modificar_almacentipo','almacentipoController@modificarAlmacentipo');
Route::post('insertar_almacentipo','almacentipoController@insertarAlmacentipo');

//--MGALMACENTIPO--//
Route::put('cargar_almacentipo','almacentipoController@cargarAlmacentipo');
Route::put('listar_almacentipo','almacentipoController@listarAlmacentipo');
Route::post('modificar_almacentipo','almacentipoController@modificarAlmacentipo');
Route::post('insertar_almacentipo','almacentipoController@insertarAlmacentipo');




