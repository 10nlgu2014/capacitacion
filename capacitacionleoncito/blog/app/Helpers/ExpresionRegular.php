<?
namespace app\Helpers;


class ExpresionRegular{
    const EXPRG_RDESCINGLES="/ÑÁÉÍÓÚñáéíóú/";//expresiones regulares para palabras en ingles
    //artisan serveconst EXPRG_NUMERO="/^[0-9]+$/";//
    const EXPRG_UUIDUSUARIO="/^[a-z0-9-]+$/";//EXPRESION REGULAR PARA VALIDAR EL UUID DEL USUARIO
    const EXPRG_PFIJOTEL="/^[a-z-A-Z0-9\+]+$/";//EXPRESION REGULAR PARA VALIDAR EL PREFIJO TELEFONICO
    const EXPREG_IDCOMPANIA="/^[A-Z0-9]+$/";
    const EXPREG_TOKEN="/^[a-zA-Z0-9.\s- ]+$/";//EXPRESION REGULAR PARA VALIDAR TOKEN CON ESPACIO Y CARACTERES ESPECIALES
    const EXPREG_TELF="/^[0-9.()\s\--]+$/";  //EXPRESION REGULAR PARA VALIDAR TELEFONO
    const EXPREG_EMAIL="/\b[\W.!#$%&*+?/\=?{|}]+@[\W-]+(?:\.[\W])\b/";// EMAIL
    const EXPREG_USUARIO="/^[a-z0-9_-]{3,16}+$/";// VALIDAR USUSARIO
    const EXPREG_PASSWORD="/(?=^.{6,60}$)((?=^.*\W$)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[|!\"#$%&\/\(\)\?\^\'\\\+\-\*]))^.*/";
    //1 MAYUSCULA, 1 MINUSCULA,1 NUMERO,1 CARACTER ESPECAIL
    const EXPREG_ALPHA="/[0-9](?=^.{1,60}$)/";//RESTRINGE NUMEROS
    const EXPREG_NUMERO = "/^[0-9]/";
    const EXPREG_ALFABETICO = "/^[A-Za-zÑÁÉÍÓÚÜ\\.\/()-_\s]+$/";
    const EXPREG_ALFANUMERICO = "/^[A-Z0-9ÑÁÉÍÓÚÜ\\.\/()-_\s]+$/";

}   