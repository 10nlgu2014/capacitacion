<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Aplicacion\Service\Ubigeo\Commands\InsertarUbigeoCommand;
    use Aplicacion\Service\Ubigeo\Commands\ListarUbigeoCommand;
    use Aplicacion\Service\Ubigeo\Commands\ModificarUbigeoCommand;
    use Aplicacion\Service\Ubigeo\Commands\CargarUbigeoCommand;

    use Aplicacion\Service\Ubigeo\Requests\ubigeoRequest;
  
    use Infraestructure\Bus\Contracts\CommandBus;

    Class ubigeoController extends Controller
    {
        private $commandBuss;
        public function __construct(CommandBus $commandBus)
        {
            $this->commandBuss=$commandBus;
        }
        
        public function listarUbigeo(Request $request)
        {
           
           
            $req=new UbigeoRequest($request);
            //dd($req);
                    $esvalido=$req->ValidarListar();
                   if (!empty($esvalido)){
                   return response($esvalido);
                   }

            $command=new ListarUbigeoCommand(
                
            $req->getId(),
            $req->getIdpais(),
            $req->getIdubigeo(),
            $req->getCodigo(),
            $req->getNivel(),
            $req->getCodigoiso(),
            $req->getDescripcioncorta(),
            $req->getDescripcionlarga(),
            $req->getTienedetalle(),
            $req->getEstado(),
            $req->getAltaaplicacion(),
            $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }
        public function modificarUbigeo(Request $request)
        {
            $req=new ubigeoRequest($request);
           
            $esvalido=$req->ValidarModificar();
            if (!empty($esvalido)){
            return response($esvalido);
            }
            

            $command=new ModificarUbigeoCommand(
            
            $req->getidpais(),
            $req->getidubigeo(),
            $req->getcodigo(),
            $req->getnivel(),
            $req->getcodigoiso(),
            $req->getdescripcioncorta(),
            $req->getdescripcionlarga(),
            $req->gettienedetalle(),
            $req->getestado(),
            $req->getaltaaplicacion(),
            $req->getusuario()
         );
            $rs=$this->commandBuss->execute($command);
            return $this->response($rs);
            return response(['datos'=>$rs]);
        }

        public function insertarUbigeo(Request $request)
        {
            $req=new ubigeoRequest($request);
           
            $esvalido=$req->ValidarInsertar();
            if (!empty($esvalido)){
            return response($esvalido);
            }

            $command=new InsertarUbigeoCommand(
            
            $req->getidpais(),
            $req->getidubigeo(),
            $req->getcodigo(),
            $req->getnivel(),
            $req->getcodigoiso(),
            $req->getdescripcioncorta(),
            $req->getdescripcionlarga(),
            $req->gettienedetalle(),
            $req->getestado(),
            $req->getaltaaplicacion(),
            $req->getusuario()
         );
            $rs=$this->commandBuss->execute($command);
            return $this->response($rs);
            return response(['datos'=>$rs]);
        }


        public function cargarUbigeo(Request $request)
        {
            $req=new ubigeoRequest($request);
           
            $esvalido=$req->ValidarCargar();
            if (!empty($esvalido)){
            return response($esvalido);
            }

            $command=new CargarUbigeoCommand(
            
    
                $req->getusuario()


            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }
               
    }

?>