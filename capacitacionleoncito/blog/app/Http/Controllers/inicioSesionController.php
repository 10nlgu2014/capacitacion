<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Aplicacion\Service\Pais\Commands\ListarPaisCommand;
    use Aplicacion\Service\Pais\Commands\InsertarPaisCommand;
    use Aplicacion\Service\Pais\Commands\CargarPaisCommand;
    use Aplicacion\Service\Pais\Commands\ModificarPaisCommand;
    use Aplicacion\Service\Pais\Requests\PaisRequest;



   use Infraestructure\Bus\Contracts\CommandBus;
    Class inicioSesionController extends Controller
    {   


        private $commandBuss;
        public function __construct(CommandBus $commandBus)
        {
            $this->commandBuss=$commandBus;
        }
        
        public function listarPais(Request $request)
        {
            
                $req=new PaisRequest($request);
         // dd($req);
                 $esvalido=$req->ValidarListar();
                if (!empty($esvalido)){
                return response($esvalido);
                }
         
                $command=new ListarPaisCommand(
                
                $req->getId(),
                $req->getCodigo(),
                $req->getDescripcioncorta(),
                $req->getDescripcionlarga(),
                $req->getNombreiso(),
                $req->getNacionalidad(),
                $req->getPrefijotelefonico(),
                $req->getFormatotelefonico(),
                $req->getNombredocumentoidentidad(),
                $req->getSigladocumentoidentidad(),
                $req->getEstado()

            );
            $rs=$this->commandBuss->execute($command);
         // dd($command);
            return response(['datos'=>$rs]);
            
        }

        public function modificarPais(Request $request)
        {
            $req=new PaisRequest($request);
           
            $esvalido=$req->ValidarModificar();
            if (!empty($esvalido)){
                return response($esvalido);
            }
                    
            $command=new ModificarPaisCommand(
 
                $req->getId(),
                $req->getDescripcioncorta(),
                $req->getDescripcionlarga(),
                $req->getNombreiso(),
                $req->getNacionalidad(),
                $req->getPrefijotelefonico(),
                $req->getFormatotelefonico(),
                $req->getNombredocumentoidentidad(),
                $req->getSigladocumentoidentidad(),
                $req->getLongituddocumentoidentidad(),
                $req->getEstado(),
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }

        public function insertarPais(Request $request)
        {
            $req=new PaisRequest($request);
           
         $esvalido=$req->ValidarInsertar();
         if (!empty($esvalido)){
             return response($esvalido);
         }
            $command=new InsertarPaisCommand(
              
                $req->getCodigo(),
                $req->getDescripcioncorta(),
                $req->getDescripcionlarga(),
                $req->getNombreiso(),
                $req->getNacionalidad(),
                $req->getPrefijotelefonico(),
                $req->getFormatotelefonico(),
                $req->getNombredocumentoidentidad(),
                $req->getSigladocumentoidentidad(),
                $req->getLongituddocumentoidentidad(),
                $req->getEstado(),
                $req->getAltaaplicacion(),
                $req->getUsuario()
            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }


        public function cargarPais(Request $request)
        {
            $req=new PaisRequest($request);
             $esvalido=$req->ValidarCargar();
            if (!empty($esvalido)){
                return response($esvalido);
             }
   
            $command=new CargarPaisCommand(
 
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            
            return response(['datos'=>$rs]);
        }
    

    public function prueba()
    {
        return view ('prueba');

    }
}

?>