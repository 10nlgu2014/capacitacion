<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Aplicacion\Service\Sexo\Commands\CargarSexoCommand;
    use Aplicacion\Service\Sexo\Commands\InsertarSexoCommand;
    use Aplicacion\Service\Sexo\Commands\ModificarSexoCommand;
    use Aplicacion\Service\Sexo\Commands\ListarSexoCommand;


    use Aplicacion\Service\Sexo\Requests\sexoRequest;
    use Infraestructure\Bus\Contracts\CommandBus;

    Class sexoController extends Controller
    {
        private $commandBuss;
        public function __construct(CommandBus $commandBus)
        {
            $this->commandBuss=$commandBus;
        }
        
        public function listarSexo(Request $request)
        {
            
            $req=new sexoRequest($request);
          // dd($req);
          $esvalido=$req->ValidarListar();
          if (!empty($esvalido)){
          return response($esvalido);
          }

            $command=new ListarSexoCommand(
                $req->getId(),
                $req->getCodigo(),
                $req->getDescripcion(),
                $req->getEstado(),
                $req->getUsuario()
                            
            );
           // dd($command);
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }

        public function modificarSexo(Request $request)
        {
            $req=new sexoRequest($request);
           
            $base=[];
            foreach($req as $r){
                $base[]=[
                    'estado'=>$r->estado,
                    'mensaje'=>$r->mensaje
                ];
            }
            return response(['datos'=>$base]);
            
            $command=new ModificarSexoCommand(
                $request->getCodigo(),
                $request->getDescripcion(),
                $request->getEstado(),
                $request->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }

        public function insertarSexo(Request $request)
        {
            $req=new sexoRequest($request);
           
            $base=[];
            foreach($req as $r){
                $base[]=[
                    'estado'=>$r->estado,
                    'mensaje'=>$r->mensaje
                ];
            }
            return response(['datos'=>$base]);

            $command=new InsertarSexoCommand(
                 
                $request->getId(),
                $request->getCodigo(),
                $request->getDescripcion(),
                $request->getEstado(),
                $request->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }
        public function cargarSexo(Request $request)
        {
            $req=new sexoRequest($request);
           
          $base=[];
            foreach($req as $r){
                $base[]=[
                    'estado'=>$r->estado,
                    'mensaje'=>$r->mensaje
                ];
            }
            return response(['datos'=>$base]);

            $command=new CargarSexoCommand(
 
                $request->getId(),
                $request->getCodigo(),
                $request->getDescripcion(),
                $request->getEstado()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }
    }

?>