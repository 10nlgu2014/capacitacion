<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Aplicacion\Service\Usuario\Commands\ListarUsuarioCommand;
    use Aplicacion\Service\Usuario\Commands\InsertarUsuarioCommand;
    use Aplicacion\Service\Usuario\Commands\CargarUsuarioCommand;
    use Aplicacion\Service\Usuario\Commands\ModificarUsuarioCommand;
    use Aplicacion\Service\Usuario\Requests\UsuarioRequest;
   use Infraestructure\Bus\Contracts\CommandBus;
    Class usuarioController extends Controller
    {   
        private $commandBuss;
        public function __construct(CommandBus $commandBus)
        {
            $this->commandBuss=$commandBus;
        }

    
        public function listarUsuario(Request $request)
        {
            
                $req=new UsuarioRequest($request);
         //dd($req);
                 $esvalido=$req->ValidarListar();
                if (!empty($esvalido)){
                return response($esvalido);
                }
         
                $command=new ListarUsuarioCommand(
 

                    $req->getid(),
                    $req->getidpersona(),
                    $req->getnombre(),
                    $req->getclave(),
                    $req->getexpiraclaveflag(),
                    $req->getfechaexpiracion(),
                    $req->getultimologin(),
                    $req->getestado(),
                    $req->getaltaaplicacion(),
                    $req->getusuario()

            );
            $rs=$this->commandBuss->execute($command);
        // dd($command);
            return response(['datos'=>$rs]);
            
        }

        public function modificarUsuario(Request $request)
        {
            $req=new UsuarioRequest($request);
           
            $esvalido=$req->ValidarModificar();
            if (!empty($esvalido)){
                return response($esvalido);
            }
                    
            $command=new ModificarUsuarioCommand(
 
                    $req->getid(),
                    $req->getidpersona(),
                    $req->getnombre(),
                    $req->getclave(),
                    $req->getexpiraclaveflag(),
                    $req->getfechaexpiracion(),
                    $req->getultimologin(),
                    $req->getestado(),
                    $req->getaltaaplicacion(),
                    $req->getusuario()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }

        public function insertarUsuario(Request $request)
        {
            $req=new UsuarioRequest($request);
           
         $esvalido=$req->ValidarInsertar();
         if (!empty($esvalido)){
             return response($esvalido);
         }
            $command=new InsertarUsuarioCommand(
              
                    $req->getid(),
                    $req->getidpersona(),
                    $req->getnombre(),
                    $req->getclave(),
                    $req->getexpiraclaveflag(),
                    $req->getfechaexpiracion(),
                    $req->getultimologin(),
                    $req->getestado(),
                    $req->getaltaaplicacion(),
                    $req->getusuario()
            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }


        public function cargarUsuario(Request $request)
        {
            $req=new UsuarioRequest($request);
             $esvalido=$req->ValidarCargar();
            if (!empty($esvalido)){
                return response($esvalido);
             }
   
            $command=new CargarUsuarioCommand(
 
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            
            return response(['datos'=>$rs]);
        }
    

    public function index()
    {
        return view ('index');

    }
    public function ubigeo()
    {
        return view ('ubigeo.nuevoubigeo');

    }
    public function paisin()
    {
        return view ('pais.insertarpais');

    }
}

?>