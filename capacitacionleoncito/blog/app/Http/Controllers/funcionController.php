<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Aplicacion\Service\Funcion\Commands\CargarFuncionCommand;
    use Aplicacion\Service\Funcion\Commands\InsertarFuncionCommand;
    use Aplicacion\Service\Funcion\Commands\ListarFuncionCommand;
    use Aplicacion\Service\Funcion\Commands\ModificarFuncionCommand;
    use Aplicacion\Service\Funcion\Requests\funcionRequest;
    use Infraestructure\Bus\Contracts\CommandBus;

    Class funcionController extends Controller
    {
     
        private $commandBuss;
        public function __construct(CommandBus $commandBus)
        {
            $this->commandBuss=$commandBus;
        }
        
        public function listarFuncion(Request $request)
        {
            $req=new funcionRequest($request);
           
             $esvalido=$req->ValidarListar();
              if (!empty($esvalido)){
                    return response($esvalido);
                }
                

            $command=new listarFuncionCommand(
               
                $request->getId(),
                $request->getCodigo(),
                $request->getDescripcion(),
                $request->getComentario(),
                $request->getEstado(),
                $request->getAltaaplicacion(),
                $request->getUsuario()
            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }
        public function modificarFuncion(Request $request)
        {
            $req=new funcionRequest($request);
           
            return response(['datos'=>$req]);

            $command=new modificarFuncionCommand(
               
                $request->getId(),
                $request->getCodigo(),
                $request->getDescripcion(),
                $request->getComentario(),
                $request->getEstado(),
                $request->getAltaaplicacion(),
                $request->getUsuario()
            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }

        public function insertarFuncion(Request $request)
        {
            $req=new funcionRequest($request);
    
            return response(['datos'=>$req]);
            $command=new insertarFuncionCommand(

                $request->getCodigo(),
                $request->getDescripcion(),
                $request->getComentario(),
                $request->getEstado(),
                $request->getAltaaplicacion(),
                $request->getUsuario()
            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
           
        }

        public function cargarFuncion(Request $request)
        {
            $req=new funcionRequest($request);
            return response(['datos'=>$req]);
           

            $command=new cargarFuncionCommand(

                $request->getUsusario()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
     
        }
    }

    ?>