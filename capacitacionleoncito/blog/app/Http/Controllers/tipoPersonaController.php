<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Aplicacion\Service\Tipopersona\Commands\ListarTipopersonaCommand;
    use Aplicacion\Service\Tipopersona\Commands\InsertarTipopersonaCommand;
    use Aplicacion\Service\Tipopersona\Commands\CargarTipopersonaCommand;
    use Aplicacion\Service\Tipopersona\Commands\ModificarTipopersonaCommand;
    use Aplicacion\Service\Tipopersona\Requests\TipopersonaRequest;
   use Infraestructure\Bus\Contracts\CommandBus;
    Class tipopersonaController extends Controller
    {   
        private $commandBuss;
        public function __construct(CommandBus $commandBus)
        {
            $this->commandBuss=$commandBus;
        }

    
        public function listarTipopersona(Request $request)
        {
            
                $req=new TipopersonaRequest($request);
         //dd($req);
                 $esvalido=$req->ValidarListar();
                if (!empty($esvalido)){
                return response($esvalido);
                }
         
                $command=new ListarTipopersonaCommand(
                
                $req->getId(),
                $req->getCodigo(),
                $req->getDescripcion(),
                $req->getEstado(),
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
         // dd($command);
            return response(['datos'=>$rs]);
            
        }

        public function modificarTipopersona(Request $request)
        {
            $req=new TipopersonaRequest($request);
           
            $esvalido=$req->ValidarModificar();
            if (!empty($esvalido)){
                return response($esvalido);
            }
                    
            $command=new ModificarTipopersonaCommand(
 
                $req->getId(),
                $req->getCodigo(),
                $req->getDescripcion(),
                $req->getEstado(),
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }

        public function insertarTipopersona(Request $request)
        {
            $req=new TipopersonaRequest($request);
           
         $esvalido=$req->ValidarInsertar();
         if (!empty($esvalido)){
             return response($esvalido);
         }
            $command=new InsertarTipopersonaCommand(
              
                $req->getId(),
                $req->getCodigo(),
                $req->getDescripcion(),
                $req->getEstado(),
                $req->getUsuario()
            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }


        public function cargarTipopersona(Request $request)
        {
            $req=new TipopersonaRequest($request);
             $esvalido=$req->ValidarCargar();
            if (!empty($esvalido)){
                return response($esvalido);
             }
   
            $command=new CargarTipopersonaCommand(
 
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            
            return response(['datos'=>$rs]);
        }
    

    public function paislistar()
    {
        return view ('pais.listarpais');

    }
    public function ubigeo()
    {
        return view ('ubigeo.nuevoubigeo');

    }
    public function paisnuevo()
    {
        return view ('pais.nuevopais');

    }
}

?>