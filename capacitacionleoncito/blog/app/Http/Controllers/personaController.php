<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Aplicacion\Service\Persona\Commands\ModificarPersonaCommand;
    use Aplicacion\Service\Persona\Commands\CargarPersonaCommand;
    use Aplicacion\Service\Persona\Commands\InsertarPersonaCommand;
    use Aplicacion\Service\Persona\Commands\ListarPersonaCommand;
    use Aplicacion\Service\Persona\Requests\PersonaRequest;
   use Infraestructure\Bus\Contracts\CommandBus;
    Class personaController extends Controller
    {   
        private $commandBuss;
        public function __construct(CommandBus $commandBus)
        {
            $this->commandBuss=$commandBus;
        }

    
        public function listarPersona(Request $request)
        {
            
                $req=new PersonaRequest($request);
         //dd($req);
                 $esvalido=$req->ValidarListar();
                if (!empty($esvalido)){
                return response($esvalido);
                }
         
                $command=new ListarPersonaCommand(
             

                $req->getId(),
                $req->getIdpais(),
                $req->getIdubigeo(),
                $req->getIdtipopersona(),
                $req->getIdsexo(),
                $req->getApellidopaterno(),
                $req->getApellidomaterno(),
                $req->getNombres(),
                $req->getNombrecompleto(),
                $req->getFechanacimiento(),
                $req->getCorreoelectronicoempresarial(),
                $req->getDireccion(),
                $req->getNumerodocumento(),
                $req->getTelefono(),
                $req->getEstado(),
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
         // dd($command);
            return response(['datos'=>$rs]);
            
        }

        public function modificarPersona(Request $request)
        {
            $req=new PersonaRequest($request);
           //dd($req);
            $esvalido=$req->ValidarModificar();
            if (!empty($esvalido)){
                return response($esvalido);
            }
                    
            $command=new ModificarPersonaCommand(
 
                $req->getId(),
                $req->getIdpais(),
                $req->getIdubigeo(),
                $req->getIdtipopersona(),
                $req->getIdsexo(),
                $req->getApellidopaterno(),
                $req->getApellidomaterno(),
                $req->getNombres(),
                $req->getNombrecompleto(),
                $req->getFechanacimiento(),
                $req->getCorreoelectronicoempresarial(),
                $req->getDireccion(),
                $req->getNumerodocumento(),
                $req->getTelefono(),
                $req->getEstado(),
                $req->getAltaaplicacion(),
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            return response(['datos'=>$rs]);
        }

        public function insertarPersona(Request $request)
        {
            $req=new PersonaRequest($request);
           //dd($req);
         $esvalido=$req->ValidarInsertar();
         if (!empty($esvalido)){
             return response($esvalido);
         }
            $command=new InsertarPersonaCommand(


                $req->getIdpais(),
                $req->getIdubigeo(),
                $req->getIdtipopersona(),
                $req->getIdsexo(),
                $req->getApellidopaterno(),
                $req->getApellidomaterno(),
                $req->getNombres(),
                $req->getNombrecompleto(),
                $req->getFechanacimiento(),
                $req->getCorreoelectronicoempresarial(),
                $req->getDireccion(),
                $req->getNumerodocumento(),
                $req->getTelefono(),
                $req->getEstado(),
                $req->getAltaaplicacion(),
                $req->getUsuario()
            );
            //dd($command);
            $rs=$this->commandBuss->execute($command);
            //dd($rs);
            return response(['datos'=>$rs]);
        }


        public function cargarPersona(Request $request)
        {
            $req=new PersonaRequest($request);
             $esvalido=$req->ValidarCargar();
            if (!empty($esvalido)){
                return response($esvalido);
             }
   
            $command=new CargarPersonaCommand(
 
                $req->getUsuario()

            );
            $rs=$this->commandBuss->execute($command);
            
            return response(['datos'=>$rs]);
        }
    

   
}

?>