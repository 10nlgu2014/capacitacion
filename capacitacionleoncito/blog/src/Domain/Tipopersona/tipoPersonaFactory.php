<?php
namespace Domain\Tipopersona;

use Domain\Tipopersona\tipopersona as Tipopersona;

class tipopersonaFactory
{
    public function cargarTipopersona($usuario)
    {
        return new Tipopersona(
            $usuario
        );
    }
    public function listarTipopersona($id,$codigo,$descripcion,$estado,$usuario

    )
    {
        return new Tipopersona(


            $id,
            $codigo,
            $descripcion,
            $estado,
            $usuario
        );
    }
    public function modificarTipopersona($id,$descripcion,$usuario
        )
    {
        return new Tipopersona(
 
            $id,
            $descripcion,
            $usuario
  
          );
    }
    public function insertarTipopersona($codigo,$descripcion,$estado,$altaaplicacion,$usuario
    )
    {
        return new Tipopersona(
            
            $codigo,
            $descripcion,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
}

?>