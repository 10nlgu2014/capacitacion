<?php
namespace Domain\Tipopersona;

use Domain\Tipopersona\tipopersona as Tipopersona;

interface tipopersonaRepositoryInterface{
    public function cargarTipopersona(Tipopersona $tipopersona);
    public function listarTipopersona(Tipopersona $tipopersona);
    public function insertarTipopersona(Tipopersona $tipopersona);
    public function modificarTipopersona(Tipopersona $tipopersona);
}