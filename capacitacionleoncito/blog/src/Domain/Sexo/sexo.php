<?php
    namespace Domain\Sexo;
    class sexo
        {
            private $id;
            private $codigo;
            private $descripcion;
            private $estado;
            private $altaaplciacion;
            private $usuario;

            public function __construct($id=null,$codigo=null,$descripcion=null,$altaaplicacion=null,$estado=null,$usuario=null
                )
            {

                $this->id=$id;
                $this->codigo=$codigo;
                $this->descripcion=$descripcion;
                $this->estado=$estado;
                $this->altaaplicacion=$altaaplicacion;
                $this->usuario=$usuario;
            }
            
            public function getId(){return $this->id;}
            public function getCodigo(){return $this->codigo;}
            public function getDescripcion(){return $this->descripcion;}
            public function getEstado(){return $this->estado;}
            public function getAltaaplicacion(){return $this->altaaplicacion;}            
            public function getUsuario(){return $this->usuario;}
            
        }

?>