<?php
namespace Domain\Sexo;

use Domain\Sexo\sexo as Sexo;

interface sexoRepositoryInterface{
    public function cargarSexo(Sexo $sexo);
    public function listarSexo(Sexo $sexo);
    public function insertarSexo(Sexo $sexo);
    public function modificarSexo(Sexo $sexo);
}

?>