<?php

namespace Domain\Sexo;

use Domain\Sexo\sexo as Sexo;

class sexoFactory
{
    public function cargarSexo($usuario)
    {
        return new Sexo(
            $usuario
        );
    }
    public function listarSexo(
        $id,
        $codigo,
        $descripcion,
        $estado,
        $usuario
    ) {
        return new Sexo(

            $id,
            $codigo,
            $descripcion,
            $estado,
            $usuario
        );
    }
    public function modificarSexo(
        $id,
        $codigo,
        $descripcion,
        $estado,
        $altaaplicacion,
        $usuario
    ) {
        return new Sexo(
            $id,
            $codigo,
            $descripcion,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
    public function insertarSexo(
        $codigo,
        $descripcion,
        $estado,
        $altaaplicacion,
        $usuario
    ) {
        return new Sexo(
            null,
            $codigo,
            $descripcion,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
}
