<?php
namespace Domain\Ubigeo;

use Domain\Ubigeo\ubigeo as Ubigeo;

class ubigeoFactory
{
    public function cargarUbigeo($usuario)
    {
        return new Ubigeo(
            $usuario
        );
    }
    public function listarUbigeo($id,$idpais,$idubigeo,$codigo,$nivel,$codigoiso,$descripcioncorta,$descripcionlarga,$tienedetalle,$estado,$altaaplicacion,$usuario
        )
    {
        return new Ubigeo(
            $id,
            $idpais,
            $idubigeo,
            $codigo,
            $nivel,
            $codigoiso,
            $descripcioncorta,
            $descripcionlarga,
            $tienedetalle,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
    public function modificarUbigeo($idpais,$idubigeo,$codigo,$nivel,$codigoiso,$descripcioncorta,$descripcionlarga,$tienedetalle,$estado,$altaaplicacion,$usuario
    )
    {
        return new Ubigeo(
            null,
            $idpais,
            $idubigeo,
            $codigo,
            $nivel,
            $codigoiso,
            $descripcioncorta,
            $descripcionlarga,
            $tienedetalle,
            $estado,
            $altaaplicacion,
            $usuario
          );
    }
    public function insertarUbigeo($idpais,$idubigeo,$codigo,$nivel,$codigoiso,$descripcioncorta,$descripcionlarga,$tienedetalle,$estado,$altaaplicacion,$usuario
    )
    {
        return new Ubigeo(
            null,
            $idpais,
            $idubigeo,
            $codigo,
            $nivel,
            $codigoiso,
            $descripcioncorta,
            $descripcionlarga,
            $tienedetalle,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
}

?>