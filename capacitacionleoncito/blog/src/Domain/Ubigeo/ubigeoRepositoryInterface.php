<?php
namespace Domain\Ubigeo;

use Domain\Ubigeo\ubigeo as Ubigeo;

interface ubigeoRepositoryInterface{
    public function cargarUbigeo(Ubigeo $ubigeo);
    public function listarUbigeo(Ubigeo $ubigeo);
    public function insertarUbigeo(Ubigeo $ubigeo);
    public function modificarUbigeo(Ubigeo $ubigeo);
}

?>