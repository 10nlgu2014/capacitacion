<?php
    namespace Domain\Pais;
    class pais
        {
            private $id;
            private $codigo;
            private $descripcioncorta;
            private $descripcionlarga;
            private $nombreiso;
            private $nacionalidad;
            private $prefijotelefonico;
            private $formatotelefonico;
            private $nombredocumentoidentidad;
            private $sigladocumentoidentidad;
            private $longituddocumentoidentidad;
            private $estado;
            private $altaaplicacion;
            private $usuario;

            public function __construct($id=null,$codigo=null,$descripcioncorta=null,$descripcionlarga=null,$nombreiso=null,$nacionalidad=null,$prefijotelefonico=null,$formatotelefonico=null,$nombredocumentoidentidad=null,$sigladocumentoidentidad=null,$longituddocumentoidentidad=null,$altaaplicacion=null,$estado=null,$usuario=null
                )
            {
                $this->id=$id;
                $this->codigo=$codigo;
                $this->descripcioncorta=$descripcioncorta;
                $this->descripcionlarga=$descripcionlarga;
                $this->nombreiso=$nombreiso;
                $this->nacionalidad=$nacionalidad;
                $this->prefijotelefonico=$prefijotelefonico;
                $this->formatotelefonico=$formatotelefonico;
                $this->nombredocumentoidentidad=$nombredocumentoidentidad;
                $this->sigladocumentoidentidad=$sigladocumentoidentidad;
                $this->longituddocumentoidentidad=$longituddocumentoidentidad;
                $this->estado=$estado;
                $this->altaaplicacion=$altaaplicacion;
                $this->usuario=$usuario;
            }
            
            public function getId()
            {
                return $this->id;
            }
            public function getCodigo()
            {
                return $this->codigo;
            }
            public function getDescripcioncorta()
            {
                return $this->descripcioncorta;
            }
            public function getDescripcionlarga()
            {
                return $this->descripcionlarga;
            }
            public function getNombreiso()
            {
                return $this->nombreiso;
            }
            public function getNacionalidad()
            {
                return $this->nacionalidad;
            }
            public function getPrefijotelefonico()
            {
                return $this->prefijotelefonico;
            }
            public function getFormatotelefonico()
            {
                return $this->formatotelefonico;
            }
            public function getNombredocumentoidentidad()
            {
                return $this->nombredocumentoidentidad;
            }
            public function getSigladocumentoidentidad()
            {
                return $this->sigladocumentoidentidad;
            }
            public function getLongituddocumentoidentidad()
            {
                return $this->longituddocumentoidentidad;
            }
            public function getEstado()
            {
                return $this->estado;
            }
            public function getAltaaplicacion()
            {
                return $this->altaaplicacion;
            }
            public function getUsuario()
            {
                return $this->usuario;
            }
            
        }

?>