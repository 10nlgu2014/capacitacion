<?php
namespace Domain\Pais;

use Domain\Pais\pais as Pais;

class paisFactory
{
    public function cargarPais($usuario)
    {
        return new Pais(
            $usuario
        );
    }
    public function listarPais(
        $id,$codigo,$descripcioncorta,$descripcionlarga,$nombreiso,$nacionalidad,$prefijotelefonico,$formatotelefonico,$nombredocumentoidentidad,$sigladocumentoidentidad,$longituddocumentoidentidad,$estado,$usuario
    )
    {
        return new Pais(
            $id,
            $codigo,
            $descripcioncorta,
            $descripcionlarga,
            $nombreiso,
            $nacionalidad,
            $prefijotelefonico,
            $formatotelefonico,
            $nombredocumentoidentidad,
            $sigladocumentoidentidad,
            $longituddocumentoidentidad,
            $estado,
            $usuario
        );
    }
    public function modificarPais($id,$descripcioncorta,$descripcionlarga,$nombreiso,$nacionalidad,$prefijotelefonico,$formatotelefonico,$nombredocumentoidentidad,$sigladocumentoidentidad,$longituddocumentoidentidad,$estado,$usuario
    )
    {
        return new Pais(
            $id,
            null,
            $descripcioncorta,
            $descripcionlarga,
            $nombreiso,
            $nacionalidad,
            $prefijotelefonico,
            $formatotelefonico,
            $nombredocumentoidentidad,
            $sigladocumentoidentidad,
            $longituddocumentoidentidad,
            $estado,
            null,
            $usuario  
          );
    }
    public function insertarPais(
    $codigo,$descripcioncorta,$descripcionlarga,$nombreiso,$nacionalidad,$prefijotelefonico,$formatotelefonico,$nombredocumentoidentidad,$sigladocumentoidentidad,$longituddocumentoidentidad,$estado,$altaaplicacion,$usuario
    )
    {
        return new Pais(
        null,
        $codigo,
        $descripcioncorta,
        $descripcionlarga,
        $nombreiso,
        $nacionalidad,
        $prefijotelefonico,
        $formatotelefonico,
        $nombredocumentoidentidad,
        $sigladocumentoidentidad,
        $longituddocumentoidentidad,
        $estado,
        $altaaplicacion,
        $usuario
        );
    }
}

?>