<?php
namespace Domain\Pais;

use Domain\Pais\pais as Pais;

interface paisRepositoryInterface{
    public function cargarPais(Pais $pais);
    public function listarPais(Pais $pais);
    public function insertarPais(Pais $pais);
    public function modificarPais(Pais $pais);
}

?>