<?php
namespace Domain\Usuario;

use Domain\Usuario\usuario as Usuario;

interface usuarioRepositoryInterface{
    public function cargarUsuario(Usuario $usuario);
    public function listarUsuario(Usuario $usuario);
    public function insertarUsuario(Usuario $usuario);
    public function modificarUsuario(Usuario $usuario);
}

?>