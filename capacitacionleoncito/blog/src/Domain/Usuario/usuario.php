<?php
    namespace Domain\Usuario;
    class usuario
        {
            private $id;
            private $idpersona;
            private $nombre;
            private $clave;
            private $expiraclaveflag;
            private $fechaexpiracion;
            private $ultimologin;
            private $estado;
            private $altaaplicacion;
            private $usuario;

            public function __construct($id=null,$idpersona=null,$nombre=null,$clave=null,$expiraclaveflag=null,$fechaexpiracion=null,$ultimologin=null,$estado=null,$altaaplicacion=null,$usuario=null

                )
            {

                $this->id=$id;
                $this->idpersona=$idpersona;
                $this->nombre=$nombre;
                $this->clave=$clave;
                $this->expiraclaveflag=$expiraclaveflag;
                $this->fechaexpiracion=$fechaexpiracion;
                $this->ultimologin=$ultimologin;
                $this->estado=$estado;
                $this->altaaplicacion=$altaaplicacion;
                $this->usuario=$usuario;


            }
            
public function getId(){
    return $this->id;
}
public function getIdpersona(){
    return $this->idpersona;
}
public function getNombre(){
    return $this->nombre;
}
public function getClave(){
    return $this->clave;
}
public function getExpiraclaveflag(){
    return $this->expiraclaveflag;
}
public function getFechaexpiracion(){
    return $this->fechaexpiracion;
}
public function getUltimologin(){
    return $this->ultimologin;
}
public function getEstado(){
    return $this->estado;
}
public function getAltaaplicacion(){
    return $this->altaaplicacion;
}
public function getUsuario(){
    return $this->usuario;
}
            
        }

?>