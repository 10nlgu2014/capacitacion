<?php
namespace Domain\Usuario;

use Domain\Usuario\usuario as Usuario;

class usuarioFactory
{
    public function cargarUsuario($usuario)
    {
        return new Usuario(
            $usuario
        );
    }
    public function listarUsuario($id,$idpersona,$nombre,$clave,$expiraclaveflag,$fechaexpiracion,$ultimologin,$estado,$altaaplicacion,$usuario
    )
    {
        return new Usuario(
            $id,
            $idpersona,
            $nombre,
            $clave,
            $expiraclaveflag,
            $fechaexpiracion,
            $ultimologin,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
    public function modificarUsuario($idpersona,$nombre,$clave,$expiraclaveflag,$fechaexpiracion,$ultimologin,$estado,$altaaplicacion,$usuario
        )
    {
        return new Usuario(
            null,
            $idpersona,
            $nombre,
            $clave,
            $expiraclaveflag,
            $fechaexpiracion,
            $ultimologin,
            $estado,
            $altaaplicacion,
            $usuario
          );
    }
    public function insertarUsuario($idpersona,$nombre,$clave,$expiraclaveflag,$fechaexpiracion,$ultimologin,$estado,$altaaplicacion,$usuario
        )
    {
        return new Usuario(

            null,
            $idpersona,
            $nombre,
            $clave,
            $expiraclaveflag,
            $fechaexpiracion,
            $ultimologin,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
}

?>