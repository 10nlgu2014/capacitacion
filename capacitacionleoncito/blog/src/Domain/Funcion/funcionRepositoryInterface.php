<?php
namespace Domain\Funcion;

use Domain\Funcion\funcionFactory as funcionFactory;

use Domain\Funcion\funcion as Funcion;

interface funcionRepositoryInterface{
    public function cargarfuncion(Funcion $funcion);
    public function listarfuncion(Funcion $funcion);
    public function modificarfuncion(Funcion $funcion);
    public function insertarfuncion(Funcion $funcion);
}

?>