<?php
namespace Domain\Funcion;

use Domain\Funcion\funcion as Funcion;

class funcionFactory
{
    public function cargarFuncion($usuario)
    {
        return new Funcion(
            $usuario
        );
    }
    public function listarFuncion($id,$codigo,$descripcion,$comentario,$estado,$altaaplicacion,$usuario
    )
    {
        return new Funcion(
            $id,
            $codigo,
            $descripcion,
            $comentario,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
    public function modificarFuncion($id,$codigo,$descripcion,$comentario,$estado,$altaaplicacion,$usuario
    )
    {
        return new Funcion(
            $id,
            $codigo,
            $descripcion,
            $comentario,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
    public function insertarFuncion($codigo,$descripcion,$comentario,$estado,$altaaplicacion,$usuario
    )
    {
        return new Funcion(
            $codigo,    
            $descripcion,
            $comentario,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
}

?>