<?php
    namespace Domain\Funcion;
    class funcion
        {
            private $id;
            private $codigo;
            private $descripcion;
            private $comentario;
            private $estado;
            private $altaaplicacion;
            private $usuario;
        

            public function __construct($id=null,$codigo=null,$descripcion=null,$comentario=null,$estado=null,$altaaplicacion=null,$usuario=null  
                )
                {
                    $this->id=$id;
                    $this->codigo=$codigo;
                    $this->descripcion=$descripcion;
                    $this->comentario=$comentario;
                    $this->estado=$estado;
                    $this->altaaplicacion=$altaaplicacion;
                    $this->usuario=$usuario;
                }

                    public function getId()
                    {
                        return $this->id;
                    }
                    public function getCodigo()
                    {
                        return $this->codigo;
                    }
                    public function getDescripcion()
                    {
                        return $this->descripcion;
                    }
                    public function getComentario()
                    {
                        return $this->comentario;
                    }
                    public function getEstado()
                    {
                        return $this->estado;
                    }
                    public function getAltaaplicacion()
                    {
                        return $this->altaaplicacion;
                    }
                    public function getUsuario()
                    {
                        return $this->usuario;
                    }


                
        }
 ?>       