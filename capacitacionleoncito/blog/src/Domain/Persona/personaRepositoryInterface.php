<?php
namespace Domain\Persona;

use Domain\Persona\persona as Persona;

interface personaRepositoryInterface{
    public function cargarPersona(Persona $persona);
    public function listarPersona(Persona $persona);
    public function insertarPersona(Persona $persona);
    public function modificarPersona(Persona $persona);
}

?>