<?php
namespace Domain\Persona;

use Domain\Persona\persona as Persona;

class personaFactory
{
    public function cargarPersona($usuario)
    {
        return new Persona(
            $usuario
        );
    }
    public function listarPersona($id,$idpais,$idubigeo,$idtipopersona,$idsexo,$apellidopaterno,$apellidomaterno,$nombres,$nombrecompleto,$fechanacimiento,$correoelectronicoempresarial,$direccion,$numerodocumento,$telefono,$estado,$usuario
    )
    {
        return new Persona(

                $id,
                $idpais,
                $idubigeo,
                $idtipopersona,
                $idsexo,
                $apellidopaterno,
                $apellidomaterno,
                $nombres,
                $nombrecompleto,
                $fechanacimiento,
                $correoelectronicoempresarial,
                $direccion,
                $numerodocumento,
                $telefono,
                $estado,
                $usuario
             );
    }
    public function modificarPersona($id,$idpais,$idubigeo,$idtipopersona,$idsexo,$apellidopaterno,$apellidomaterno,$nombres,$nombrecompleto,$fechanacimiento,$correoelectronicoempresarial,$direccion,$numerodocumento,$telefono,$estado,$altaaplicacion,$usuario
    )
    {
        return new Persona(
            $id,
            $idpais,
            $idubigeo,
            $idtipopersona,
            $idsexo,
            $apellidopaterno,
            $apellidomaterno,
            $nombres,
            $nombrecompleto,
            $fechanacimiento,
            $correoelectronicoempresarial,
            $direccion,
            $numerodocumento,
            $telefono,
            $estado,
            $altaaplicacion,
            $usuario
          );
    }
    public function insertarPersona($idpais,$idubigeo,$idtipopersona,$idsexo,$apellidopaterno,$apellidomaterno,$nombres,$nombrecompleto,$fechanacimiento,$correoelectronicoempresarial,$direccion,$numerodocumento,$telefono,$estado,$altaaplicacion,$usuario
    )
    {
        return new Persona(

            $idpais,
            $idubigeo,
            $idtipopersona,
            $idsexo,
            $apellidopaterno,
            $apellidomaterno,
            $nombres,
            $nombrecompleto,
            $fechanacimiento,
            $correoelectronicoempresarial,
            $direccion,
            $numerodocumento,
            $telefono,
            $estado,
            $altaaplicacion,
            $usuario
        );
    }
}

?>