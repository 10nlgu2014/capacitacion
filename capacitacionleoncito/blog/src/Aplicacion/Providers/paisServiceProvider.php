<?php
namespace Aplicacion\Providers;

use Domain\Pais\paisRepositoryInterface;
use Illuminate\Support\ServiceProvider;

use Infraestructure\Bus\Contracts\CommandBus;
use Infraestructure\Bus\Contracts\Container;
use Infraestructure\Bus\LaravelContainer;
use Infraestructure\Bus\SimpleCommandBus;

use Infraestructure\Repositories\PaisPgRepository;


class paisServiceProvider extends ServiceProvider
{
    protected $defer = false;
    public function bot()
    {
    }

    public function register()
    {
        $this->app->bind(
            CommandBus::class,
            SimpleCommandBus::class

        );

        $this->app->bind(
            Container::class,
            LaravelContainer::class
        );

        $this->app->bind(
            paisRepositoryInterface::class,
            PaisPgRepository::class
        );
    }
}
