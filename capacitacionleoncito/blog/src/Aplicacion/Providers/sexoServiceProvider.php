<?php
namespace Aplicacion\Providers;

use Domain\Sexo\sexoRepositoryInterface;
use Illuminate\Support\ServiceProvider;

use Infraestructure\Bus\Contracts\CommandBus;
use Infraestructure\Bus\Contracts\Container;
use Infraestructure\Bus\LaravelContainer;
use Infraestructure\Bus\SimpleCommandBus;

use Infraestructure\Repositories\SexoPgRepository;

class sexoServiceProvider extends ServiceProvider{
    protected $defer = false;
    public function bot()
    {
    }

    public function register()
    {
        $this->app->bind(
            CommandBus::class,
            SimpleCommandBus::class

        );

        $this->app->bind(
            Container::class,
            LaravelContainer::class
        );

        $this->app->bind(
            sexoRepositoryInterface::class,
            SexoPgRepository::class
        );
    }
}