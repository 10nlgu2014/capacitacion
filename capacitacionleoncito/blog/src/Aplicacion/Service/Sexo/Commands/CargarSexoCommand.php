<?php
namespace Aplicacion\Service\Sexo\Commands;
use Aplicacion\Service\Contracts\Command;

class CargarSexoCommand implements Command
{

    private $usuario;

    public function __construct($usuario=null
    )
    {

        $this->usuario=$usuario;
    }


        public function getUsuario()
        {
            return $this->usuario;
        }
}