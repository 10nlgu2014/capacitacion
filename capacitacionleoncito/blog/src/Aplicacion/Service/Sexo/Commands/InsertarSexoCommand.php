<?php
namespace Aplicacion\Service\Sexo\Commands;
use Aplicacion\Service\Contracts\Command;

class InsertarSexoCommand implements Command
{

    private $codigo;
    private $descripcion;
    private $estado;
    private $altaaplicacion;
    private $usuario;

    public function __construct($id=null,$codigo=null,$descripcion=null,$estado=null,$altaaplicacion=null,$usuario=null
    )
    {
        
        $this->codigo=$codigo;
        $this->descripcion=$descripcion;
        $this->estado=$estado;
        $this->altaaplicacion=$altaaplicacion;
        $this->usuario=$usuario;
    }

        public function getId()
        {
            return $this->id;
        }
        public function getCodigo()
        {
            return $this->codigo;
        }
        public function getDescripcion()
        {
            return $this->descripcion;
        }
        public function getEstado()
        {
            return $this->estado;
        }
        public function getAltaaplicacion(){
            return $this->altaaplicacion;
        }
        public function getUsuario()
        {
            return $this->usuario;
        }
}