<?php 
    namespace Aplicacion\Service\Sexo\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Sexo\sexoFactory;
    use Domain\Sexo\sexoRepositoryInterface;

class ModificarSexoHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(sexoFactory $factory,sexoRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
            $arrayData=[];
            $sexoFactory=$this->factory->listarSexo(

            $command->getId(),
            $command->getCodigo(),
            $command->getDescripcion(),
            $command->getEstado(),
            $command->getAltaaplicacion(),
            $command->getUsuario()  

            );
            $rs=$this->repository->modificarSexo($sexoFactory);
            return ['estado' => $rs[0]->estadoflag, 'mensaje' => $rs[0]->mensaje];
        }
    }
    ?>



