<?php 
    namespace Aplicacion\Service\Sexo\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Sexo\sexoFactory;
    use Domain\Sexo\sexoRepositoryInterface;

class CargarSexoHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(sexoFactory $factory,sexoRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
            $arrayData=[];
            $sexoFactory=$this->factory->cargarSexo(

          
            $command->getUsuario()  

            );
            $rs=$this->repository->cargarSexo($sexoFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   

                    'id'=>$r->mgsexo_id,
                    'codigo'=>$r->mgsexo_codigo,
                    'descripcion'=>$r->mgsexo_descripcion,
                    'estado'=>$r->mgsexo_estado,
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



