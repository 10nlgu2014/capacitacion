<?php
namespace Aplicacion\Service\Tipopersona\Commands;

use Aplicacion\Service\Contracts\Command;


class CargarTipopersonaCommand implements Command
{
    
    private $usuario;

    public function __construct($usuario=null
    ){
    
    $this->usuario=$usuario;
    }

    
    public function getUsuario()
    {
        return $this->usuario;
    }
}