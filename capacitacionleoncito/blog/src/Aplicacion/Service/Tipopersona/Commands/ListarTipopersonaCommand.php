<?php
namespace Aplicacion\Service\Tipopersona\Commands;

use Aplicacion\Service\Contracts\Command;


class ListarTipopersonaCommand implements Command
{

            private $id;
            private $codigo;
            private $descripcion;
            private $estado;
            private $usuario;

    public function __construct($id=null,$codigo=null,$descripcion=null,$estado=null,$usuario=null
        ){

            $this->id=$id;
            $this->codigo=$codigo;
            $this->descripcion=$descripcion;
            $this->estado=$estado;
            $this->usuario=$usuario;
    }


        public function getId(){return $this->id;}
        public function getCodigo(){return $this->codigo;}
        public function getDescripcion(){return $this->descripcion;}
        public function getEstado(){return $this->estado;}
        public function getUsuario(){return $this->usuario;}
}