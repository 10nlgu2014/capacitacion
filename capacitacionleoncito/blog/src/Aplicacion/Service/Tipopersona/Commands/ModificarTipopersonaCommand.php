<?php
namespace Aplicacion\Service\Tipopersona\Commands;

use Aplicacion\Service\Contracts\Command;


class ModificarTipopersonaCommand implements Command
{


        private $id;
        private $descripcion;
        private $usuario;



    public function __construct($id=null,$descripcion=null,$usuario=null)
        { 
        $this->id=$id;
        $this->descripcion=$descripcion;
        $this->usuario=$usuario;
    }


        public function getId(){return $this->id;}
        public function getDescripcion(){return $this->descripcion;}
        public function getUsuario(){return $this->usuario;}
}