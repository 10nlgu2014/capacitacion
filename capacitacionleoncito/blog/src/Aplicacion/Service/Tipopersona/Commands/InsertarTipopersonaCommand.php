<?php
namespace Aplicacion\Service\Tipopersona\Commands;

use Aplicacion\Service\Contracts\Command;


class InsertarTipopersonaCommand implements Command
{

            private $codigo;
            private $descripcion;
            private $estado;
            private $altaaplicacion;
            private $usuario;

    public function __construct($codigo=null,$descripcion=null,$estado=null,$altaaplicacion=null,$usuario=null
        ){


            $this->codigo=$codigo;
            $this->descripcion=$descripcion;
            $this->estado=$estado;
            $this->altaaplicacion=$altaaplicacion;
            $this->usuario=$usuario;
    }



            public function getCodigo(){return $this->codigo;}
            public function getDescripcion(){return $this->descripcion;}
            public function getEstado(){return $this->estado;}
            public function getAltaaplicacion(){return $this->altaaplicacion;}
            public function getUsuario(){return $this->usuario;}
    
}