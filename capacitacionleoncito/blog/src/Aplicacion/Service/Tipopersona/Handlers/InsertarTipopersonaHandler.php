<?php 
    namespace Aplicacion\Service\Tipopersona\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Tipopersona\tipopersonaFactory;
    use Domain\Tipopersona\tipopersonaRepositoryInterface;

class InsertarTipopersonaHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(tipopersonaFactory $factory,tipopersonaRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
          //dd($command);
            $arrayData=[];
            $tipopersonaFactory=$this->factory->insertarTipopersona(
  

                $command->getCodigo(),
                $command->getDescripcion(),
                $command->getEstado(),
                $command->getAltaaplicacion(),
                $command->getUsuario()
                
            );
           
            $rs=$this->repository->insertarTipopersona($tipopersonaFactory);
            return ['estado' => $rs[0]->estadoflag, 'mensaje' => $rs[0]->mensaje];
    }
}
    ?>



