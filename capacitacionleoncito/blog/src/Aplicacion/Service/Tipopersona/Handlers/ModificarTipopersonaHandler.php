<?php 
    namespace Aplicacion\Service\Tipopersona\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Tipopersona\tipopersonaFactory;
    use Domain\Tipopersona\tipopersonaRepositoryInterface;

class ModificarTipopersonaHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(tipopersonaFactory $factory,tipopersonaRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $tipopersonaFactory=$this->factory->modificarTipopersona(
  
                    $command->getId(),
                    $command->getDescripcion(),
                    $command->getUsuario()
                
            );
           
            $rs=$this->repository->modificarTipopersona($tipopersonaFactory);
            return ['estado' => $rs[0]->estadoflag, 'mensaje' => $rs[0]->mensaje];
        }
    }
    ?>



