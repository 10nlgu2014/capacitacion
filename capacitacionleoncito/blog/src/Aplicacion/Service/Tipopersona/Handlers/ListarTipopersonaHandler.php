<?php 
    namespace Aplicacion\Service\Tipopersona\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Tipopersona\tipopersonaFactory;
    use Domain\Tipopersona\tipopersonaRepositoryInterface;

class ListarTipopersonaHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(tipopersonaFactory $factory,tipopersonaRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $tipopersonaFactory=$this->factory->listarTipopersona(

            $command ->getId(),
            $command ->getCodigo(),
            $command ->getDescripcion(),
            $command ->getEstado(),
            $command ->getUsuario()
                
            );
           
            $rs=$this->repository->listarTipopersona($tipopersonaFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   

                'pid'=>$r->mgtipopersona_pid,
                'pcodigo'=>$r->mgtipopersona_pcodigo,
                'pdescripcion'=>$r->mgtipopersona_pdescripcion,
                'pestado'=>$r->mgtipopersona_pestado,
                'pusuario'=>$r->mgtipopersona_pusuario,                    
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



