<?php 
    namespace Aplicacion\Service\Tipopersona\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Tipopersona\tipopersonaFactory;
    use Domain\Tipopersona\tipopersonaRepositoryInterface;

class CargarTipopersonaHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(tipopersonaFactory $factory,tipopersonaRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $tipopersonaFactory=$this->factory->cargarTipopersona(

            $command->getUsuario()
                
            );
           
            $rs=$this->repository->cargartipopersona($tipopersonaFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   

                'id'=>$r->mgtipopersona_id,
                'codigo'=>$r->mgtipopersona_codigo,
                'descripcion'=>$r->mgtipopersona_descripcion,
                'estado'=>$r->mgtipopersona_estado,
                'altaaplicacion'=>$r->mgtipopersona_altaaplicacion,
                    
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



