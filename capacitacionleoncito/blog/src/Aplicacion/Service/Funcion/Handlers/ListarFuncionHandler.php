<?php 
    namespace Aplicacion\Service\Funcion\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Funcion\funcionFactory;
    use Domain\Funcion\funcionRepositoryInterface;

class ListarFuncionHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(funcionFactory $factory,funcionRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
            $arrayData=[];
            $funcionFactory=$this->factory->listarFuncion(

                $command->getId(),
                $command->getCodigo(),
                $command->getDescripcion(),
                $command->getComentario(),
                $command->getEstado(),
                $command->getAltaaplicacion(),
                $command->getUsuario()
                
            );
            $rs=$this->repository->listarFuncion($funcionFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[
                                              
                       
                        'id'=>$r->sefuncion_id,
                        'codigo'=>$r->sefuncion_codigo,
                        'descripcion'=>$r->sefuncion_descripcion,
                        'comentario'=>$r->sefuncion_comentario,
                        'estado'=>$r->sefuncion_estado,
                        'altaaplicacion'=>$r->sefuncion_altaaplicacion,

                    ]);
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>
