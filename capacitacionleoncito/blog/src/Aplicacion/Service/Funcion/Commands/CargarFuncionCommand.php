<?php
namespace Aplicacion\Service\Funcion\Commands;
use Aplicacion\Service\Contracts\Command;

class CargarFuncionCommand implements Command
{
    private $usuario;

    public function __construct($usuario=null
    ){
        $this->usuario=$usuario;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }
}