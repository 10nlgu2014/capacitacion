<?php
namespace Aplicacion\Service\Funcion\Requests;

// use App\Helpers\Helpers\ExpresionRegular as ExpresionRegular;
// use Aplicacion\Service\Version\TokenRequest;
// use Application\Service\TokenRequest as ServiceTokenRequest;

class funcionRequest {
    private $request;
    private $id;
    private $codigo;    
    private $descripcion;
    private $comentario;
    private $estado;
    private $altaaplicacion;
    private $usuario;

    public function __construct($request)
    {
        $this->request=$request;
      //  parent::__construct($request->bearerToken());
    }

        public function getId()
        {
            return $this->id;
        }  
            public function getCodigo()
            {
                return $this->codigo;
            }    
            public function getDescripcion()
            {
                return $this->descripcion;
            }
            public function getComentario()
            {
                return $this->comentario;
            }
            public function getEstado()
            {
                return $this->estado;
            }
            public function getAltaaplicacion()
            {
                return $this->altaaplicacion;
            }
            public function getUsuario()
            {
                return $this->usuario;
            }

            public function ValidarListar(){
       
                // if(!$this->request->has('codigo')){
                //     return ['estado'=>false,'El codigo es requerido.'];
                // }
                // if(!$this->request->has('descripcion')){
                //     return ['estado'=>false,'La descripcion es requerido.'];
                // }

                // if(!$this->request->has('estado')){
                //     return ['estado'=>false,'El estado es requerido.'];
                // }
                // if(!$this->request->has('altaaplicacion')){
                //     return ['estado'=>false,'La altaaplicacion es requerida.'];
                // }

                $this->id=$this->request->input("id");                
                $this->codigo=$this->request->input("codigo");
                $this->descripcion=$this->request->input("descripcion");
                $this->comentario=$this->request->input("comentario");
                $this->estado=$this->request->input("estado");
                $this->altaaplicacion=$this->request->input("altaaplicacion");
                $this->usuario=$this->request->input("usuario");
                return [];

                
            }
        }
?>