<?php 
    namespace Aplicacion\Service\Pais\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Pais\paisFactory;
    use Domain\Pais\paisRepositoryInterface;

class ListarPaisHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(paisFactory $factory,paisRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $paisFactory=$this->factory->listarPais(
                
            $command->getId(),
            $command->getCodigo(),
            $command->getDescripcioncorta(),
            $command->getDescripcionlarga(),
            $command->getNombreiso(),
            $command->getNacionalidad(),
            $command->getPrefijotelefonico(),
            $command->getFormatotelefonico(),
            $command->getNombredocumentoidentidad(),
            $command->getSigladocumentoidentidad(),
            $command->getLongituddocumentoidentidad(),
            $command->getEstado(),
            $command->getUsuario()
                
            );
           
            $rs=$this->repository->listarPais($paisFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   
                        
                    'id'=>$r->mgpais_id,
                    'codigo'=>$r->mgpais_codigo,
                    'descripcioncorta'=>$r->mgpais_descripcioncorta,
                    'descripcionlarga'=>$r->mgpais_descripcionlarga,
                    'nombreiso'=>$r->mgpais_nombreiso,
                    'nacionalidad'=>$r->mgpais_nacionalidad,
                    'prefijotelefonico'=>$r->mgpais_prefijotelefonico,
                    'formatotelefonico'=>$r->mgpais_formatotelefonico,
                    'nombredocumentoidentidad'=>$r->mgpais_nombredocumentoidentidad,
                    'sigladocumentoidentidad'=>$r->mgpais_sigladocumentoidentidad,
                    'longituddocumentoidentidad'=>$r->mgpais_longituddocumentoidentidad,
                    'estado'=>$r->mgpais_estado,
                    
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



