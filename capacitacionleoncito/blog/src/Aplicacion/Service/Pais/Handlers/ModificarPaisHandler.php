<?php 
    namespace Aplicacion\Service\Pais\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Pais\paisFactory;
    use Domain\Pais\paisRepositoryInterface;

class ModificarPaisHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(paisFactory $factory,paisRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
          //dd($command);
            $arrayData=[];
            $paisFactory=$this->factory->modificarPais(
                
            $command->getId(),
            $command->getDescripcioncorta(),
            $command->getDescripcionlarga(),
            $command->getNombreiso(),
            $command->getNacionalidad(),
            $command->getPrefijotelefonico(),
            $command->getFormatotelefonico(),
            $command->getNombredocumentoidentidad(),
            $command->getSigladocumentoidentidad(),
            $command->getLongituddocumentoidentidad(),
            $command->getEstado(),
            $command->getUsuario()
                
            );
           
            $rs=$this->repository->modificarPais($paisFactory);
            return ['estado' => $rs[0]->estadoflag, 'mensaje' => $rs[0]->mensaje];
    }
}
    ?>



