<?php 
    namespace Aplicacion\Service\Pais\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Pais\paisFactory;
    use Domain\Pais\paisRepositoryInterface;

class CargarPaisHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(paisFactory $factory,paisRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $paisFactory=$this->factory->cargarPais(

            $command->getUsuario()
                
            );
           
            $rs=$this->repository->cargarPais($paisFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   
                        
                    'id'=>$r->mgpais_id,
                    'codigo'=>$r->mgpais_codigo,
                    'descripcioncorta'=>$r->mgpais_descripcioncorta,
                    'estado'=>$r->mgpais_estado,
                    
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



