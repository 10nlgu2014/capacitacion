<?php
namespace Aplicacion\Service\Pais\Commands;

use Aplicacion\Service\Contracts\Command;


class ModificarPaisCommand implements Command
{
    private $id;
    private $descripcioncorta;
    private $descripcionlarga;
    private $nombreiso;
    private $nacionalidad;
    private $prefijotelefonico;
    private $formatotelefonico;
    private $nombredocumentoidentidad;
    private $sigladocumentoidentidad;
    private $longituddocumentoidentidad;
    private $estado;
    private $usuario;

    public function __construct($id=null,$descripcioncorta=null,$descripcionlarga=null,$nombreiso=null,$nacionalidad=null,$prefijotelefonico=null,$formatotelefonico=null,$nombredocumentoidentidad=null,$sigladocumentoidentidad=null,$longituddocumentoidentidad=null,$estado=null,$usuario=null
    ){
  
    $this->id=$id;
    $this->descripcioncorta=$descripcioncorta;
    $this->descripcionlarga=$descripcionlarga;
    $this->nombreiso=$nombreiso;
    $this->nacionalidad=$nacionalidad;
    $this->prefijotelefonico=$prefijotelefonico;
    $this->formatotelefonico=$formatotelefonico;
    $this->nombredocumentoidentidad=$nombredocumentoidentidad;
    $this->sigladocumentoidentidad=$sigladocumentoidentidad;
    $this->longituddocumentoidentidad=$longituddocumentoidentidad;
    $this->estado=$estado;
    $this->usuario=$usuario;
    }


    public function getId()
    {
        return $this->id;
    }
    public function getDescripcioncorta()
    {
        return $this->descripcioncorta;
    }
    public function getDescripcionlarga()
    {
        return $this->descripcionlarga;
    }
    public function getNombreiso()
    {
        return $this->nombreiso;
    }
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }
    public function getPrefijotelefonico()
    {
        return $this->prefijotelefonico;
    }
    public function getFormatotelefonico()
    {
        return $this->formatotelefonico;
    }
    public function getNombredocumentoidentidad()
    {
        return $this->nombredocumentoidentidad;
    }
    public function getSigladocumentoidentidad()
    {
        return $this->sigladocumentoidentidad;
    }
    public function getLongituddocumentoidentidad()
    {
        return $this->longituddocumentoidentidad;
    }
    public function getEstado()
    {
        return $this->estado;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }
}