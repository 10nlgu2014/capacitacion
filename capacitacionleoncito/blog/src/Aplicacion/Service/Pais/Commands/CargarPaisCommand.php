<?php
namespace Aplicacion\Service\Pais\Commands;

use Aplicacion\Service\Contracts\Command;


class CargarPaisCommand implements Command
{
    
    private $usuario;

    public function __construct($usuario=null
    ){
    
    $this->usuario=$usuario;
    }

    
    public function getUsuario()
    {
        return $this->usuario;
    }
}