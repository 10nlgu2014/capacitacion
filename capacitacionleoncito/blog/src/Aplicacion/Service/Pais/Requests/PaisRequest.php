<?php
namespace Aplicacion\Service\Pais\Requests;

use App\Helpers\Helpers\ExpresionRegular;
use Aplicacion\Service\Version\TokenRequest;
//use Application\Service\TokenRequest as ServiceTokenRequest;


class PaisRequest{
    private $request;
    private $id;
    private $codigo;
    private $descripcioncorta;
    private $descripcionlarga;
    private $nombreiso;
    private $nacionalidad;
    private $prefijotelefonico;
    private $formatotelefonico;
    private $nombredocumentoidentidad;
    private $sigladocumentoidentidad;
    private $longituddocumentoidentidad;
    private $estado;
    private $altaaplicacion;
    private $usuario;

    public function __construct($request)
    {

        $this->request=$request;
        //parent::__construct($request->bearerToken());
        
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }
    
    public function getdescripcioncorta()
    {
        return $this->descripcioncorta;
    }
    public function getdescripcionlarga()
    {
        return $this->descripcionlarga;
    }
    public function getnombreiso()
    {
        return $this->nombreiso;
    }
    public function getnacionalidad()
    {
        return $this->nacionalidad;
    }
    public function getprefijotelefonico()
    {
        return $this->prefijotelefonico;
    }
    public function getformatotelefonico()
    {
        return $this->formatotelefonico;
    }
    public function getnombredocumentoidentidad()
    {
        return $this->nombredocumentoidentidad;
    }
    public function getsigladocumentoidentidad()
    {
        return $this->sigladocumentoidentidad;
    }
    public function getlongituddocumentoidentidad()
    {
        return $this->longituddocumentoidentidad;
    }
    public function getestado()
    {
        return $this->estado;
    }
    public function getAltaaplicacion()
    {
        return $this->altaaplicacion;
    }
    
    public function getUsuario()
    {
        return $this->usuario;
    }
    
    public function ValidarListar(){
       

       //dd($this->request);
        $this->id=$this->request->input("id");
        $this->codigo=$this->request->input("codigo");
        $this->descripcioncorta=$this->request->input("descripcioncorta");
        $this->descripcionlarga=$this->request->input("descripcionlarga");
        $this->nombreiso=$this->request->input("nombreiso");
        $this->nacionalidad=$this->request->input("nacionalidad");
        $this->prefijotelefonico=$this->request->input("prefijotelefonico");
        $this->formatotelefonico=$this->request->input("formatotelefonico");
        $this->nombredocumentoidentidad=$this->request->input("nombredocumentoidentidad");
        $this->sigladocumentoidentidad=$this->request->input("sigladocumentoidentidad");
        $this->longituddocumentoidentidad=$this->request->input("longituddocumentoidentidad");
        $this->estado=$this->request->input("estado");
        $this->usuario=$this->request->input("usuario");
        return[];

        // if(!$this->request->has('codigo')){
        //     return ['estado'=>false,'El codigo es requerido.'];
        // }
        // if(!$this->request->has('descripcioncorta')){
        //     return ['estado'=>false,'La descripcioncorta es requerida.'];
        // }
        // if(!$this->request->has('descripcionlarga')){
        //     return ['estado'=>false,'La descripcionlarga es requerida.'];
        // }
        // if(!$this->request->has('nombreiso')){
        //     return ['estado'=>false,'El nombreiso es requerido.'];
        // }
        // if(!$this->request->has('nacionalidad')){
        //     return ['estado'=>false,'La nacionalidad es requerida.'];
        // }
        // if(!$this->request->has('prefijotelefonico')){
        //     return ['estado'=>false,'El prefijotelefonico es requerida.'];
        // }
        // if(!$this->request->has('formatotelefonico')){
        //     return ['estado'=>false,'El formatotelefonico es requerido.'];
        // }
        // if(!$this->request->has('nombredocumentoidentidad')){
        //     return ['estado'=>false,'El nombredocumentoidentidad es requerido.'];
        // }   
        // if(!$this->request->has('usuario')){
        //     return ['estado'=>false,'El usuario es requerido.'];
        // }


        // if (!preg_match('/^[0-9]+$/',$this->codigo)){
        //     return ['estado'=>false,'El codigo debe ser numérico.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,50}$)/',$this->descripcioncorta)){
        //     return ['estado'=>false,'La descripcion no debe contener números y su longitud máxima es de 50 caracteres.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,80}$)/',$this->descripcionlarga)){
        //     return ['estado'=>false,'La descripcion no debe contener números y su longitud máxima es de 80 caracteres.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,30}$)/',$this->nacionalidad)){
        //     return ['estado'=>false,'La nacionalidad no debe contener números y su longitud máxima es de 30 caracteres.'];
        // }
        // if (!preg_match('/^[a-z-A-Z0-9\+]+$/',$this->prefijotelefonico)){
        //     return ['estado'=>false,'El prefijo tiene otro formato.'];
        // }
        
     }

     public function ValidarInsertar(){
       
        //dd($this->request);
        $this->codigo=$this->request->input("codigo");
        $this->descripcioncorta=$this->request->input("descripcioncorta");
        $this->descripcionlarga=$this->request->input("descripcionlarga");
        $this->nombreiso=$this->request->input("nombreiso");
        $this->nacionalidad=$this->request->input("nacionalidad");
        $this->prefijotelefonico=$this->request->input("prefijotelefonico");
        $this->formatotelefonico=$this->request->input("formatotelefonico");
        $this->nombredocumentoidentidad=$this->request->input("nombredocumentoidentidad");
        $this->sigladocumentoidentidad=$this->request->input("sigladocumentoidentidad");
        $this->longituddocumentoidentidad=$this->request->input("longituddocumentoidentidad");
        $this->estado=$this->request->input("estado");
        $this->altaaplicacion=$this->request->input("altaaplicacion");
        $this->usuario=$this->request->input("usuario");
        return[];

     }
     public function ValidarModificar(){
        $this->id=$this->request->input("id");
        $this->descripcioncorta=$this->request->input("descripcioncorta");
        $this->descripcionlarga=$this->request->input("descripcionlarga");
        $this->nombreiso=$this->request->input("nombreiso");
        $this->nacionalidad=$this->request->input("nacionalidad");
        $this->prefijotelefonico=$this->request->input("prefijotelefonico");
        $this->formatotelefonico=$this->request->input("formatotelefonico");
        $this->nombredocumentoidentidad=$this->request->input("nombredocumentoidentidad");
        $this->sigladocumentoidentidad=$this->request->input("sigladocumentoidentidad");
        $this->longituddocumentoidentidad=$this->request->input("longituddocumentoidentidad");
        $this->estado=$this->request->input("estado");
        $this->usuario=$this->request->input("usuario");
        return[];

     }
     public function ValidarCargar(){

        $this->usuario=$this->request->input("usuario");
        return[];

     }

    }

    ?>