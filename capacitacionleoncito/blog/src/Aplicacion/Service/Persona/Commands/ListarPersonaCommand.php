<?php
namespace Aplicacion\Service\Persona\Commands;

use Aplicacion\Service\Contracts\Command;


class ListarPersonaCommand implements Command
{
    private $id;
    private $idpais;
    private $idubigeo;
    private $idtipopersona;
    private $idsexo;
    private $apellidopaterno;
    private $apellidomaterno;
    private $nombres;
    private $nombrecompleto;
    private $fechanacimiento;
    private $correoelectronicoempresarial;
    private $direccion;
    private $numerodocumento;
    private $telefono;
    private $estado;
    private $usuario;

    public function __construct($id=null,$idpais=null,$idubigeo=null,$idtipopersona=null,$idsexo=null,$apellidopaterno=null,$apellidomaterno=null,$nombres=null,$nombrecompleto=null,$fechanacimiento=null,$correoelectronicoempresarial=null,$direccion=null,$numerodocumento=null,$telefono=null,$estado=null,$usuario=null    ){
        $this->id=$id;
        $this->idpais=$idpais;
        $this->idubigeo=$idubigeo;
        $this->idtipopersona=$idtipopersona;
        $this->idsexo=$idsexo;
        $this->apellidopaterno=$apellidopaterno;
        $this->apellidomaterno=$apellidomaterno;
        $this->nombres=$nombres;
        $this->nombrecompleto=$nombrecompleto;
        $this->fechanacimiento=$fechanacimiento;
        $this->correoelectronicoempresarial=$correoelectronicoempresarial;
        $this->direccion=$direccion;
        $this->numerodocumento=$numerodocumento;
        $this->telefono=$telefono;
        $this->estado=$estado;
        $this->usuario=$usuario;
    }
public function getId(){return $this->id;}
public function getIdpais(){return $this->idpais;}
public function getIdubigeo(){return $this->idubigeo;}
public function getIdtipopersona(){return $this->idtipopersona;}
public function getIdsexo(){return $this->idsexo;}
public function getApellidopaterno(){return $this->apellidopaterno;}
public function getApellidomaterno(){return $this->apellidomaterno;}
public function getNombres(){return $this->nombres;}
public function getNombrecompleto(){return $this->nombrecompleto;}
public function getFechanacimiento(){return $this->fechanacimiento;}
public function getCorreoelectronicoempresarial(){return $this->correoelectronicoempresarial;}
public function getDireccion(){return $this->direccion;}
public function getNumerodocumento(){return $this->numerodocumento;}
public function getTelefono(){return $this->telefono;}
public function getEstado(){return $this->estado;}
public function getUsuario(){return $this->usuario;}
}