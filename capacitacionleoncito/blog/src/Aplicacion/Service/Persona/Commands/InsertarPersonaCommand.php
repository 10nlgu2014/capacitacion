<?php
namespace Aplicacion\Service\Persona\Commands;

use Aplicacion\Service\Contracts\Command;


class InsertarPersonaCommand implements Command
{



            private $idpais;
            private $idubigeo;
            private $idtipopersona;
            private $idsexo;
            private $apellidopaterno;
            private $apellidomaterno;
            private $nombres;
            private $nombrecompleto;
            private $fechanacimiento;
            private $correoelectronicoempresarial;
            private $direccion;
            private $numerodocumento;
            private $telefono;
            private $estado;
            private $altaaplicacion;
            private $usuario;

    public function __construct($idpais=null,$idubigeo=null,$idtipopersona=null,$idsexo=null,$apellidopaterno=null,$apellidomaterno=null,$nombres=null,$nombrecompleto=null,$fechanacimiento=null,$correoelectronicoempresarial=null,$direccion=null,$numerodocumento=null,$telefono=null,$estado=null,$altaaplicacion=null,$usuario=null
        ){

                $this->idpais=$idpais;
                $this->idubigeo=$idubigeo;
                $this->idtipopersona=$idtipopersona;
                $this->idsexo=$idsexo;
                $this->apellidopaterno=$apellidopaterno;
                $this->apellidomaterno=$apellidomaterno;
                $this->nombres=$nombres;
                $this->nombrecompleto=$nombrecompleto;
                $this->fechanacimiento=$fechanacimiento;
                $this->correoelectronicoempresarial=$correoelectronicoempresarial;
                $this->direccion=$direccion;
                $this->numerodocumento=$numerodocumento;
                $this->telefono=$telefono;
                $this->estado=$estado;
                $this->altaaplicacion=$altaaplicacion;
                $this->usuario=$usuario;
    }

    public function getidpais(){return $this->idpais;}
    public function getidubigeo(){return $this->idubigeo;}
    public function getidtipopersona(){return $this->idtipopersona;}
    public function getidsexo(){return $this->idsexo;}
    public function getapellidopaterno(){return $this->apellidopaterno;}
    public function getapellidomaterno(){return $this->apellidomaterno;}
    public function getnombres(){return $this->nombres;}
    public function getnombrecompleto(){return $this->nombrecompleto;}
    public function getfechanacimiento(){return $this->fechanacimiento;}
    public function getcorreoelectronicoempresarial(){return $this->correoelectronicoempresarial;}
    public function getdireccion(){return $this->direccion;}
    public function getnumerodocumento(){return $this->numerodocumento;}
    public function gettelefono(){return $this->telefono;}
    public function getestado(){return $this->estado;}
    public function getaltaaplicacion(){return $this->altaaplicacion;}
    public function getusuario(){return $this->usuario;}
    
}