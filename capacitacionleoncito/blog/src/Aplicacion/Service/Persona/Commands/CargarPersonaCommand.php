<?php
namespace Aplicacion\Service\Persona\Commands;

use Aplicacion\Service\Contracts\Command;


class CargarPersonaCommand implements Command
{
    
    private $usuario;

    public function __construct($usuario=null
    ){
    
    $this->usuario=$usuario;
    }

    
    public function getUsuario()
    {
        return $this->usuario;
    }
}