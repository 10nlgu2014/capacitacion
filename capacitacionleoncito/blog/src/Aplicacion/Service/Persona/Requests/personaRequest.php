<?php
namespace Aplicacion\Service\Persona\Requests;

use App\Helpers\Helpers\ExpresionRegular;
use Aplicacion\Service\Version\TokenRequest;
//use Application\Service\TokenRequest as ServiceTokenRequest;


class PersonaRequest{
    private $request;
    private $id;
    private $idpais;
    private $idubigeo;
    private $idtipopersona;
    private $idsexo;
    private $apellidopaterno;
    private $apellidomaterno;
    private $nombres;
    private $nombrecompleto;
    private $fechanacimiento;
    private $correoelectronicoempresarial;
    private $direccion;
    private $numerodocumento;
    private $telefono;
    private $estado;
    private $altaaplicacion;
    private $usuario;

    public function __construct($request)
    {

        $this->request=$request;
        //parent::__construct($request->bearerToken());
        
    }

public function getId(){return $this->id;}
public function getIdpais(){return $this->idpais;}
public function getIdubigeo(){return $this->idubigeo;}
public function getIdtipopersona(){return $this->idtipopersona;}
public function getIdsexo(){return $this->idsexo;}
public function getApellidopaterno(){return $this->apellidopaterno;}
public function getApellidomaterno(){return $this->apellidomaterno;}
public function getNombres(){return $this->nombres;}
public function getNombrecompleto(){return $this->nombrecompleto;}
public function getFechanacimiento(){return $this->fechanacimiento;}
public function getCorreoelectronicoempresarial(){return $this->correoelectronicoempresarial;}
public function getDireccion(){return $this->direccion;}
public function getNumerodocumento(){return $this->numerodocumento;}
public function getTelefono(){return $this->telefono;}
public function getEstado(){return $this->estado;}
public function getAltaaplicacion(){return $this->altaaplicacion;}
public function getUsuario(){return $this->usuario;}
    
    public function ValidarListar(){
       

       //dd($this->request);
        
    
    $this->id=$this->request->input("id");
    $this->idpais=$this->request->input("idpais");
    $this->idubigeo=$this->request->input("idubigeo");
    $this->idtipopersona=$this->request->input("idtipopersona");
    $this->idsexo=$this->request->input("idsexo");
    $this->apellidopaterno=$this->request->input("apellidopaterno");
    $this->apellidomaterno=$this->request->input("apellidomaterno");
    $this->nombres=$this->request->input("nombres");
    $this->nombrecompleto=$this->request->input("nombrecompleto");
    $this->fechanacimiento=$this->request->input("fechanacimiento");
    $this->correoelectronicoempresarial=$this->request->input("correoelectronicoempresarial");
    $this->direccion=$this->request->input("direccion");
    $this->numerodocumento=$this->request->input("numerodocumento");
    $this->telefono=$this->request->input("telefono");
    $this->estado=$this->request->input("estado");
    $this->usuario=$this->request->input("usuario");
        return[];

        // if(!$this->request->has('codigo')){
        //     return ['estado'=>false,'El codigo es requerido.'];
        // }
        // if(!$this->request->has('descripcioncorta')){
        //     return ['estado'=>false,'La descripcioncorta es requerida.'];
        // }
        // if(!$this->request->has('descripcionlarga')){
        //     return ['estado'=>false,'La descripcionlarga es requerida.'];
        // }
        // if(!$this->request->has('nombreiso')){
        //     return ['estado'=>false,'El nombreiso es requerido.'];
        // }
        // if(!$this->request->has('nacionalidad')){
        //     return ['estado'=>false,'La nacionalidad es requerida.'];
        // }
        // if(!$this->request->has('prefijotelefonico')){
        //     return ['estado'=>false,'El prefijotelefonico es requerida.'];
        // }
        // if(!$this->request->has('formatotelefonico')){
        //     return ['estado'=>false,'El formatotelefonico es requerido.'];
        // }
        // if(!$this->request->has('nombredocumentoidentidad')){
        //     return ['estado'=>false,'El nombredocumentoidentidad es requerido.'];
        // }   
        // if(!$this->request->has('usuario')){
        //     return ['estado'=>false,'El usuario es requerido.'];
        // }


        // if (!preg_match('/^[0-9]+$/',$this->codigo)){
        //     return ['estado'=>false,'El codigo debe ser numérico.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,50}$)/',$this->descripcioncorta)){
        //     return ['estado'=>false,'La descripcion no debe contener números y su longitud máxima es de 50 caracteres.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,80}$)/',$this->descripcionlarga)){
        //     return ['estado'=>false,'La descripcion no debe contener números y su longitud máxima es de 80 caracteres.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,30}$)/',$this->nacionalidad)){
        //     return ['estado'=>false,'La nacionalidad no debe contener números y su longitud máxima es de 30 caracteres.'];
        // }
        // if (!preg_match('/^[a-z-A-Z0-9\+]+$/',$this->prefijotelefonico)){
        //     return ['estado'=>false,'El prefijo tiene otro formato.'];
        // }
        
     }

     public function ValidarInsertar(){
       
        //dd($this->request);

        $this->idpais=$this->$this->request->input("idpais");
        $this->idubigeo=$this->$this->request->input("idubigeo");
        $this->idtipopersona=$this->$this->request->input("idtipopersona");
        $this->idsexo=$this->$this->request->input("idsexo");
        $this->apellidopaterno=$this->$this->request->input("apellidopaterno");
        $this->apellidomaterno=$this->$this->request->input("apellidomaterno");
        $this->nombres=$this->$this->request->input("nombres");
        $this->nombrecompleto=$this->$this->request->input("nombrecompleto");
        $this->fechanacimiento=$this->$this->request->input("fechanacimiento");
        $this->correoelectronicoempresarial=$this->$this->request->input("correoelectronicoempresarial");
        $this->direccion=$this->$this->request->input("direccion");
        $this->numerodocumento=$this->$this->request->input("numerodocumento");
        $this->telefono=$this->$this->request->input("telefono");
        $this->estado=$this->$this->request->input("estado");
        $this->altaaplicacion=$this->$this->request->input("altaaplicacion");
        $this->usuario=$this->$this->request->input("usuario");

        return[];

     }
     public function ValidarModificar(){
      
    
            $this->id=$this->request->input("id");
            $this->idpais=$this->request->input("idpais");
            $this->idubigeo=$this->request->input("idubigeo");
            $this->idtipopersona=$this->request->input("idtipopersona");
            $this->idsexo=$this->request->input("idsexo");
            $this->apellidopaterno=$this->request->input("apellidopaterno");
            $this->apellidomaterno=$this->request->input("apellidomaterno");
            $this->nombres=$this->request->input("nombres");
            $this->nombrecompleto=$this->request->input("nombrecompleto");
            $this->fechanacimiento=$this->request->input("fechanacimiento");
            $this->correoelectronicoempresarial=$this->request->input("correoelectronicoempresarial");
            $this->direccion=$this->request->input("direccion");
            $this->numerodocumento=$this->request->input("numerodocumento");
            $this->telefono=$this->request->input("telefono");
            $this->estado=$this->request->input("estado");
            $this->altaaplicacion=$this->request->input("altaaplicacion");
            $this->usuario=$this->request->input("usuario");
                return[];

     }
     public function ValidarCargar(){

        $this->usuario=$this->request->input("usuario");
        return[];

     }

    }

    ?>