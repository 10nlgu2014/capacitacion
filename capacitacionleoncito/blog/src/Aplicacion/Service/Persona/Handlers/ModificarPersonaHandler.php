<?php 
    namespace Aplicacion\Service\Persona\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Persona\personaFactory;
    use Domain\Persona\personaRepositoryInterface;

class ModificarPersonaHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(personaFactory $factory,personaRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $personaFactory=$this->factory->modificarPersona(
  

                    $command->getId(),
                    $command->getIdpais(),
                    $command->getIdubigeo(),
                    $command->getIdtipopersona(),
                    $command->getIdsexo(),
                    $command->getApellidopaterno(),
                    $command->getApellidomaterno(),
                    $command->getNombres(),
                    $command->getNombrecompleto(),
                    $command->getFechanacimiento(),
                    $command->getCorreoelectronicoempresarial(),
                    $command->getDireccion(),
                    $command->getNumerodocumento(),
                    $command->getTelefono(),
                    $command->getEstado(),
                    $command->getAltaaplicacion(),
                    $command->getUsuario()
                
            );
           
            $rs=$this->repository->modificarPersona($personaFactory);
            return ['estado' => $rs[0]->estadoflag, 'mensaje' => $rs[0]->mensaje];
        }
    }
    ?>



