<?php 
    namespace Aplicacion\Service\Persona\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Persona\personaFactory;
    use Domain\Persona\personaRepositoryInterface;

class CargarPersonaHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(personaFactory $factory,personaRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $personaFactory=$this->factory->cargarPersona(

            $command->getUsuario()
                
            );
           
            $rs=$this->repository->cargarPersona($personaFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   
                        
      

                'idpais'=>$r->mgpersona_idpais,
                'idubigeo'=>$r->mgpersona_idubigeo,
                'idtipopersona'=>$r->mgpersona_idtipopersona,
                'idsexo'=>$r->mgpersona_idsexo,
                'apellidopaterno'=>$r->mgpersona_apellidopaterno,
                'apellidomaterno'=>$r->mgpersona_apellidomaterno,
                'nombres'=>$r->mgpersona_nombres,
                'nombrecompleto'=>$r->mgpersona_nombrecompleto,
               // 'fechanacimiento'=>$r->mgpersona_fechanacimiento,
                'correoelectronicoempresarial'=>$r->mgpersona_correoelectronicoempresarial,
                'direccion'=>$r->mgpersona_direccion,
                'numerodocumento'=>$r->mgpersona_numerodocumento,
                'telefono'=>$r->mgpersona_telefono,
                'estado'=>$r->mgpersona_estado,
                    
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



