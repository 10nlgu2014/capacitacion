<?php 
    namespace Aplicacion\Service\Persona\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Persona\personaFactory;
    use Domain\Persona\personaRepositoryInterface;

class ListarPersonaHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(personaFactory $factory,personaRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $personaFactory=$this->factory->listarPersona(
  
                $command->getId(),
                $command->getIdpais(),
                $command->getIdubigeo(),
                $command->getIdtipopersona(),
                $command->getIdsexo(),
                $command->getApellidopaterno(),
                $command->getApellidomaterno(),
                $command->getNombres(),
                $command->getNombrecompleto(),
                $command->getFechanacimiento(),
                $command->getCorreoelectronicoempresarial(),
                $command->getDireccion(),
                $command->getNumerodocumento(),
                $command->getTelefono(),
                $command->getEstado(),
                $command->getUsuario()
                
            );
           
            $rs=$this->repository->listarPersona($personaFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   
  
                'pid'=>$r->mgpersona_pid,
                'pidpais'=>$r->mgpersona_pidpais,
                'pidubigeo'=>$r->mgpersona_pidubigeo,
                'pidtipopersona'=>$r->mgpersona_pidtipopersona,
                'pidsexo'=>$r->mgpersona_pidsexo,
                'papellidopaterno'=>$r->mgpersona_papellidopaterno,
                'papellidomaterno'=>$r->mgpersona_papellidomaterno,
                'pnombres'=>$r->mgpersona_pnombres,
                'pnombrecompleto'=>$r->mgpersona_pnombrecompleto,
                'pfechanacimiento'=>$r->mgpersona_pfechanacimiento,
                'pcorreoelectronicoempresarial'=>$r->mgpersona_pcorreoelectronicoempresarial,
                'pdireccion'=>$r->mgpersona_pdireccion,
                'pnumerodocumento'=>$r->mgpersona_pnumerodocumento,
                'ptelefono'=>$r->mgpersona_ptelefono,
                'pestado'=>$r->mgpersona_pestado,
               
                    
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



