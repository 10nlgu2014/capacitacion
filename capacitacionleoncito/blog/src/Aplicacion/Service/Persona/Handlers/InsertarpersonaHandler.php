<?php 
    namespace Aplicacion\Service\Persona\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Persona\personaFactory;
    use Domain\Persona\personaRepositoryInterface;

class InsertarPersonaHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(personaFactory $factory,personaRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
          //dd($command);
            $arrayData=[];
            $personaFactory=$this->factory->insertarPersona(
  

                $command->getIdpais(),
                $command->getIdubigeo(),
                $command->getIdtipopersona(),
                $command->getIdsexo(),
                $command->getApellidopaterno(),
                $command->getApellidomaterno(),
                $command->getNombres(),
                $command->getNombrecompleto(),
                $command->getFechanacimiento(),
                $command->getCorreoelectronicoempresarial(),
                $command->getDireccion(),
                $command->getNumerodocumento(),
                $command->getTelefono(),
                $command->getEstado(),
                $command->getaltaaplicacion(),
                $command->getUsuario()
                
            );
           
            $rs=$this->repository->insertarPersona($personaFactory);
            return ['estado' => $rs[0]->estadoflag, 'mensaje' => $rs[0]->mensaje];
    }
}
    ?>



