<?php
    namespace Application\Service;
    use Tymon\JWTAuth\Facades\JWTAuth;

    class TokenRequest
    {
        private $token;
        public function __construct($token=null)

        {
             $this->token=$token;

        }
        public function getUsuario()
        {
            if(!is_null($this->token)){
                $pay=JWTAuth::setToken($this->token)->getPayload();
                return $pay['nombre']['nombre'];
            }
            return null;
        }
    }
?>