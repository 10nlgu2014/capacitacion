<?php
namespace Aplicacion\Service\Ubigeo\Commands;

use Aplicacion\Service\Contracts\Command;


class CargarUbigeoCommand implements Command
{
    
    private $usuario;

    public function __construct($usuario=null
    ){
    
    $this->usuario=$usuario;
    }

    
    public function getUsuario()
    {
        return $this->usuario;
    }
}