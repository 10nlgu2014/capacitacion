<?php

namespace Aplicacion\Service\Ubigeo\Commands;

use Aplicacion\Service\Contracts\Command;

class ListarUbigeoCommand implements Command
{

    private $id;
    private $idpais;
    private $idubigeo;
    private $codigo;
    private $nivel;
    private $codigoiso;
    private $descripcioncorta;
    private $descripcionlarga;
    private $tienedetalle;
    private $estado;
    private $altaaplicacion;
    private $usuario;

    public function __construct(
        $id = null,
        $idpais = null,
        $idubigeo = null,
        $codigo = null,
        $nivel = null,
        $codigoiso = null,
        $descripcioncorta = null,
        $descripcionlarga = null,
        $tienedetalle = null,
        $estado = null,
        $altaaplicacion = null,
        $usuario = null
    ) {

        $this->id=$id;
        $this->idpais=$idpais;
        $this->idubigeo=$idubigeo;
        $this->codigo=$codigo;
        $this->nivel=$nivel;
        $this->codigoiso=$codigoiso;
        $this->descripcioncorta=$descripcioncorta;
        $this->descripcionlarga=$descripcionlarga;
        $this->tienedetalle=$tienedetalle;
        $this->estado=$estado;
        $this->altaaplicacion=$altaaplicacion;
        $this->usuario=$usuario;
    }

    public function getId()
    {
        return $this->id;
    }
    public function getIdpais()
    {
        return $this->idpais;
    }
    public function getIdubigeo()
    {
        return $this->idubigeo;
    }
    public function getCodigo()
    {
        return $this->codigo;
    }
    public function getNivel()
    {
        return $this->nivel;
    }
    public function getCodigoiso()
    {
        return $this->codigoiso;
    }
    public function getDescripcioncorta()
    {
        return $this->descripcioncorta;
    }
    public function getDescripcionlarga()
    {
        return $this->descripcionlarga;
    }
    public function getTienedetalle()
    {
        return $this->tienedetalle;
    }
    public function getEstado()
    {
        return $this->estado;
    }
    public function getAltaaplicacion()
    {
        return $this->altaaplicacion;
    }
    public function getUsuario()
    {
        return $this->usuario;
    }
}
