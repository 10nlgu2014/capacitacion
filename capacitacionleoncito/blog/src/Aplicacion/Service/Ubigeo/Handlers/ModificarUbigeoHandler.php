<?php 
    namespace Aplicacion\Service\Ubigeo\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Ubigeo\ubigeoFactory;
    use Domain\Ubigeo\ubigeoRepositoryInterface;

class ModificarUbigeoHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(ubigeoFactory $factory,ubigeoRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
          //dd($command);
            $arrayData=[];
            $ubigeoFactory=$this->factory->modificarUbigeo(
                

            $command->getId(),
            $command->getDescripcioncorta(),
            $command->getDescripcionlarga(),
            $command->getNombreiso(),
            $command->getNacionalidad(),
            $command->getPrefijotelefonico(),
            $command->getFormatotelefonico(),
            $command->getNombredocumentoidentidad(),
            $command->getSigladocumentoidentidad(),
            $command->getLongituddocumentoidentidad(),
            $command->getEstado(),
            $command->getUsuario()
                
            );
           
            $rs=$this->repository->modificarUbigeo($ubigeoFactory);
            return ['estado' => $rs[0]->estadoflag, 'mensaje' => $rs[0]->mensaje];
    }
}
    ?>



