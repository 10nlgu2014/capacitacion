<?php 
    namespace Aplicacion\Service\Ubigeo\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Ubigeo\ubigeoFactory;
    use Domain\Ubigeo\ubigeoRepositoryInterface;

class CargarUbigeoHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(ubigeoFactory $factory,ubigeoRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $ubigeoFactory=$this->factory->cargarUbigeo(

            $command->getUsuario()
                
            );
           
            $rs=$this->repository->cargarUbigeo($ubigeoFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   
                        
                    'id'=>$r->mgubigeo_idpais,
                    'idpais'=>$r->mgubigeo_idubigeo,
                    'codigo'=>$r->mgubigeo_codigo,
                    'nivel'=>$r->mgubigeo_nivel,
                    'codigoiso'=>$r->mgubigeo_codigoiso,
                    'descripcioncorta'=>$r->mgubigeo_descripcioncorta,
                    'descripcionlarga'=>$r->mgubigeo_descripcionlarga,
                    'tienedetalle'=>$r->mgubigeo_tienedetalle,
                    'estado'=>$r->mgubigeo_estado,
                 
                    
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



