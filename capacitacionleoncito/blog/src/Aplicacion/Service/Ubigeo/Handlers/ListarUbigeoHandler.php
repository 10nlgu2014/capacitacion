<?php 
    namespace Aplicacion\Service\Ubigeo\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Ubigeo\ubigeoFactory;
    use Domain\Ubigeo\ubigeoRepositoryInterface;

class ListarUbigeoHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(ubigeoFactory $factory,ubigeoRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $ubigeoFactory=$this->factory->listarUbigeo(


                $command->getId(),
                $command->getIdpais(),
                $command->getIdubigeo(),
                $command->getCodigo(),
                $command->getNivel(),
                $command->getCodigoiso(),
                $command->getDescripcioncorta(),
                $command->getDescripcionlarga(),
                $command->getTienedetalle(),
                $command->getEstado(),
                $command->getaltaaplicacion(),
                $command->getUsuario()
                
            );
           
            $rs=$this->repository->listarUbigeo($ubigeoFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   
                        

                'id'=>$r->mgubigeo_id,
                'idpais'=>$r->mgubigeo_idpais,
                'idubigeo'=>$r->mgubigeo_idubigeo,
                'codigo'=>$r->mgubigeo_codigo,
                'nivel'=>$r->mgubigeo_nivel,
                'codigoiso'=>$r->mgubigeo_codigoiso,
                'descripcioncorta'=>$r->mgubigeo_descripcioncorta,
                'descripcionlarga'=>$r->mgubigeo_descripcionlarga,
                'tienedetalle'=>$r->mgubigeo_tienedetalle,
                'estado'=>$r->mgubigeo_estado,
         
                    


                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



