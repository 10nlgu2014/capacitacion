<?php
namespace Aplicacion\Service\Usuario\Requests;
use App\Helpers\Helpers\ExpresionRegular;
use Aplicacion\Service\Version\TokenRequest;
//use Application\Service\TokenRequest as ServiceTokenRequest;


class UsuarioRequest{
    private $request;

        private $id;
        private $idpersona;
        private $nombre;
        private $clave;
        private $expiraclaveflag;
        private $fechaexpiracion;
        private $ultimologin;
        private $estado;
        private $altaaplicacion;
        private $usuario;

    public function __construct($request)
    {

        $this->request=$request;
        //parent::__construct($request->bearerToken());
        
    }
public function getid(){
    return $this->id;
}
public function getidpersona(){
    return $this->idpersona;
}
public function getnombre(){
    return $this->nombre;
}
public function getclave(){
    return $this->clave;
}
public function getexpiraclaveflag(){
    return $this->expiraclaveflag;
}
public function getfechaexpiracion(){
    return $this->fechaexpiracion;
}
public function getultimologin(){
    return $this->ultimologin;
}
public function getestado(){
    return $this->estado;
}
public function getaltaaplicacion(){
    return $this->altaaplicacion;
}
public function getusuario(){
    return $this->usuario;
}
    
    public function ValidarListar(){
       

       //dd($this->request);

        $this->id=$this->request->input(" id");
        $this->idpersona=$this->request->input("idpersona,");
        $this->nombre=$this->request->input("nombre");
        $this->clave=$this->request->input("clave");
        $this->expiraclaveflag=$this->request->input("expiraclaveflag,");
        $this->fechaexpiracion=$this->request->input("fechaexpiracion");
        $this->ultimologin=$this->request->input("ultimologin");
        $this->estado=$this->request->input("estado");
        $this->altaaplicacion=$this->request->input("altaaplicacion");
        $this->usuario=$this->request->input("usuario");
        return[];

        // if(!$this->request->has('codigo')){
        //     return ['estado'=>false,'El codigo es requerido.'];
        // }
        // if(!$this->request->has('descripcioncorta')){
        //     return ['estado'=>false,'La descripcioncorta es requerida.'];
        // }
        // if(!$this->request->has('descripcionlarga')){
        //     return ['estado'=>false,'La descripcionlarga es requerida.'];
        // }
        // if(!$this->request->has('nombreiso')){
        //     return ['estado'=>false,'El nombreiso es requerido.'];
        // }
        // if(!$this->request->has('nacionalidad')){
        //     return ['estado'=>false,'La nacionalidad es requerida.'];
        // }
        // if(!$this->request->has('prefijotelefonico')){
        //     return ['estado'=>false,'El prefijotelefonico es requerida.'];
        // }
        // if(!$this->request->has('formatotelefonico')){
        //     return ['estado'=>false,'El formatotelefonico es requerido.'];
        // }
        // if(!$this->request->has('nombredocumentoidentidad')){
        //     return ['estado'=>false,'El nombredocumentoidentidad es requerido.'];
        // }   
        // if(!$this->request->has('usuario')){
        //     return ['estado'=>false,'El usuario es requerido.'];
        // }


        // if (!preg_match('/^[0-9]+$/',$this->codigo)){
        //     return ['estado'=>false,'El codigo debe ser numérico.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,50}$)/',$this->descripcioncorta)){
        //     return ['estado'=>false,'La descripcion no debe contener números y su longitud máxima es de 50 caracteres.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,80}$)/',$this->descripcionlarga)){
        //     return ['estado'=>false,'La descripcion no debe contener números y su longitud máxima es de 80 caracteres.'];
        // }
        // if (!preg_match('/[0-9](?=^.{1,30}$)/',$this->nacionalidad)){
        //     return ['estado'=>false,'La nacionalidad no debe contener números y su longitud máxima es de 30 caracteres.'];
        // }
        // if (!preg_match('/^[a-z-A-Z0-9\+]+$/',$this->prefijotelefonico)){
        //     return ['estado'=>false,'El prefijo tiene otro formato.'];
        // }
        
     }

     public function ValidarInsertar(){
       
        //dd($this->request);
        $this->idpersona=$this->request->input("idpersona");
        $this->nombre=$this->request->input("nombre");
        $this->clave=$this->request->input("clave");
        $this->expiraclaveflag=$this->request->input("expiraclaveflag");
        $this->fechaexpiracion=$this->request->input("fechaexpiracion");
        $this->ultimologin=$this->request->input("ultimologin");
        $this->estado=$this->request->input("estado");
        $this->altaaplicacion=$this->request->input("altaaplicacion");
        $this->usuario=$this->request->input("usuario");
        return[];

     }
     public function ValidarModificar(){


            $this->idpersona=$this->request->input("idpersona");
            $this->nombre=$this->request->input("nombre");
            $this->clave=$this->request->input("clave");
            $this->expiraclaveflag=$this->request->input("expiraclaveflag");
            $this->fechaexpiracion=$this->request->input("fechaexpiracion");
            $this->ultimologin=$this->request->input("ultimologin");
            $this->estado=$this->request->input("estado");
            $this->altaaplicacion=$this->request->input("altaaplicacion");
            $this->usuario=$this->request->input("usuario");
        return[];

     }
     public function ValidarCargar(){

        $this->usuario=$this->request->input("usuario");
        return[];

     }

    }

    ?>