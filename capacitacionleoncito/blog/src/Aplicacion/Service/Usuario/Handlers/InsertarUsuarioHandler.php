<?php 
    namespace Aplicacion\Service\Usuario\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Usuario\usuarioFactory;
    use Domain\Usuario\usuarioRepositoryInterface;

class InsertarUsuarioHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(usuarioFactory $factory,usuarioRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
          //dd($command);
            $arrayData=[];
            $usuarioFactory=$this->factory->insertarUsuario(
           
    
                $command->getidpersona(),
                $command->getNombre(),
                $command->getClave(),
                $command->getExpiraclaveflag(),
                $command->getFechaexpiracion(),
                $command->getUltimologin(),
                $command->getEstado(),
                $command->getAltaaplicacion(),
                $command->getUsuario()
                
            );
           
            $rs=$this->repository->insertarUsuario($usuarioFactory);
            return ['estado' => $rs[0]->estadoflag, 'mensaje' => $rs[0]->mensaje];
    }
}
    ?>



