<?php 
    namespace Aplicacion\Service\Usuario\Handlers;
    use Aplicacion\Service\Contracts\Handler;
    use Domain\Usuario\usuarioFactory;
    use Domain\Usuario\usuarioRepositoryInterface;

class CargarUsuarioHandler implements Handler{
        private $factory;
        private $repository;
        
        public function __construct(usuarioFactory $factory,usuarioRepositoryInterface $repository)
        {
            $this->factory = $factory;
            $this->repository=$repository;   
        }
        public function __invoke($command)
        {
           
            $arrayData=[];
            $usuarioFactory=$this->factory->cargarUsuario(

            $command->getUsuario()
                
            );
           
            $rs=$this->repository->cargarUsuario($usuarioFactory);
            if($rs[0]->estadoflag){
                foreach ($rs as $r){
                    array_push($arrayData,[   

                    'nombre'=>$r->seusuario_nombre,
                    'clave'=>$r->seusuario_clave,
                    'expiraclaveflag'=>$r->seusuario_expiraclaveflag,
                    'fechaexpiracion'=>$r->seusuario_fechaexpiracion,
                    'ultimologin'=>$r->seusuario_ultimologin,
                    'estado'=>$r->seusuario_estado,
                    'altaaplicacion'=>$r->seusuario_altaaplicacion,
                    
                    ]);
                    
                }return ['estado'=>true,'data'=>$arrayData];
            }else{
                    return ["estado"=>false,"mensaje"=>$rs[0]->mensaje];
                 }
        }
    }
    ?>



