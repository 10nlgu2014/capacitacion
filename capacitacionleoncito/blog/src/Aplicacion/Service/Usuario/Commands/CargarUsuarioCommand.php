<?php
namespace Aplicacion\Service\Usuario\Commands;

use Aplicacion\Service\Contracts\Command;


class CargarUsuarioCommand implements Command
{
    
    private $usuario;

    public function __construct($usuario=null
    ){
    
    $this->usuario=$usuario;
    }

    
    public function getUsuario()
    {
        return $this->usuario;
    }
}