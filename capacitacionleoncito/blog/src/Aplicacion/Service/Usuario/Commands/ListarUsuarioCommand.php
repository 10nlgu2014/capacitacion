<?php
namespace Aplicacion\Service\Usuario\Commands;

use Aplicacion\Service\Contracts\Command;


class ListarUsuarioCommand implements Command
{

    private $id;
    private $idpersona;
    private $nombre;
    private $clave;
    private $expiraclaveflag;
    private $fechaexpiracion;
    private $ultimologin;
    private $estado;
    private $altaaplicacion;
    private $usuario;

    public function __construct(
$id=null,$idpersona=null,$nombre=null,$clave=null,$expiraclaveflag=null,$fechaexpiracion=null,$ultimologin=null,$estado=null,$altaaplicacion=null,$usuario=null
    ){

        $this->id=$id;
        $this->idpersona=$idpersona;
        $this->nombre=$nombre;
        $this->clave=$clave;
        $this->expiraclaveflag=$expiraclaveflag;
        $this->fechaexpiracion=$fechaexpiracion;
        $this->ultimologin=$ultimologin;
        $this->estado=$estado;
        $this->altaaplicacion=$altaaplicacion;
        $this->usuario=$usuario;
    }
        public function getid(){return $this->id;}
        public function getidpersona(){return $this->idpersona;}
        public function getnombre(){return $this->nombre;}
        public function getclave(){return $this->clave;}
        public function getexpiraclaveflag(){return $this->expiraclaveflag;}
        public function getfechaexpiracion(){return $this->fechaexpiracion;}
        public function getultimologin(){return $this->ultimologin;}
        public function getestado(){return $this->estado;}
        public function getaltaaplicacion(){return $this->altaaplicacion;}
        public function getusuario(){return $this->usuario;}
}