<?php

namespace Infraestructure\Bus;

use Aplicacion\Service\Contracts\Command;
use Infraestructure\Bus\Contracts\CommandBus;
use Infraestructure\Bus\Contracts\Container;

final class SimpleCommandBus implements CommandBus
{
    private const COMMAND_PREFIX = 'Command';
    private  const HANDLER_PREFIX = 'Handler';

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function execute($command)
    {
        return $this->resolveHandler($command)->__invoke($command);
    }

    private function resolveHandler(Command $command)
    {
        return $this->container->make($this->getHandlerClass($command));
    }

    private function getHandlerClass(Command $command)
    {
        return str_replace(
            self::COMMAND_PREFIX,
            self::HANDLER_PREFIX,
            get_class($command)
        );
    }
}