<?php

namespace Infraestructure\Bus\Contracts;

interface CommandBus
{
    public function execute($command);
}