<?php

namespace Infraestructure\Bus\Contracts;

interface Container
{
    public function make($class);
}