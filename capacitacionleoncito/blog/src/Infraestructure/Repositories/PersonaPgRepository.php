<?php
namespace Infraestructure\Repositories;
use Domain\Persona\personaRepositoryInterface;
use Domain\Persona\persona as Persona;
use Illuminate\Support\Facades\DB;


final class PersonaPgRepository implements personaRepositoryInterface
{
    public function listarPersona(Persona $persona)
    {
        
        $result=DB::select('
            
        select * from mgpersona_listar(
            :pid,
            :pidpais,
            :pidubigeo,
            :pidtipopersona,
            :pidsexo,
            :papellidopaterno,
            :papellidomaterno,
            :pnombres,
            :pnombrecompleto,
            :pfechanacimiento,
            :pcorreoelectronicoempresarial,
            :pdireccion,
            :pnumerodocumento,
            :ptelefono,
            :pestado,
            :pusuario
            )' ,
            [


            "pid"=>$persona->getId(),
            "pidpais"=>$persona->getIdpais(),
            "pidubigeo"=>$persona->getIdubigeo(),
            "pidtipopersona"=>$persona->getIdtipopersona(),
            "pidsexo"=>$persona->getIdsexo(),
            "papellidopaterno"=>$persona->getApellidopaterno(),
            "papellidomaterno"=>$persona->getApellidomaterno(),
            "pnombres"=>$persona->getNombres(),
            "pnombrecompleto"=>$persona->getNombrecompleto(),
            "pfechanacimiento"=>$persona->getFechanacimiento(),
            "pcorreoelectronicoempresarial"=>$persona->getCorreoelectronicoempresarial(),
            "pdireccion"=>$persona->getDireccion(),
            "pnumerodocumento"=>$persona->getNumerodocumento(),
            "ptelefono"=>$persona->getTelefono(),
            "pestado"=>$persona->getEstado(),
            "pusuario"=>$persona->getUsuario()

            ]
            );
            
                    return $result;
        }

    public function cargarPersona(Persona $persona)
    {
        $result =DB::select(
                'select* from mgpersona_cargar(:pusuario)',
                [
                    "pusuario"=>$persona->getUsuario()
                ]
        );
        
            return $result;
    }

    public function modificarPersona(Persona $persona)
    {
         //dd($pais);
         $result=DB::select('
            
         select * from mgpersona_modificar(

            :pid,
            :pidpais,
            :pidubigeo,
            :pidtipopersona,
            :pidsexo,
            :papellidopaterno,
            :papellidomaterno,
            :pnombres,
            :pnombrecompleto,
            :pfechanacimiento,
            :pcorreoelectronicoempresarial,
            :pdireccion,
            :pnumerodocumento,
            :ptelefono,
            :pestado,
            :paltaaplicacion,
            :pusuario
             )' ,
             [


                "pid"=>$persona->getId(),
                "pidpais"=>$persona->getIdpais(),
                "pidubigeo"=>$persona->getIdubigeo(),
                "pidtipopersona"=>$persona->getIdtipopersona(),
                "pidsexo"=>$persona->getIdsexo(),
                "papellidopaterno"=>$persona->getApellidopaterno(),
                "papellidomaterno"=>$persona->getApellidomaterno(),
                "pnombres"=>$persona->getNombres(),
                "pnombrecompleto"=>$persona->getNombrecompleto(),
                "pfechanacimiento"=>$persona->getFechanacimiento(),
                "pcorreoelectronicoempresarial"=>$persona->getCorreoelectronicoempresarial(),
                "pdireccion"=>$persona->getDireccion(),
                "pnumerodocumento"=>$persona->getNumerodocumento(),
                "ptelefono"=>$persona->getTelefono(),
                "pestado"=>$persona->getEstado(),
                "paltaaplicacion"=>$persona->getAltaaplicacion(),
                "pusuario"=>$persona->getUsuario(),
             ]
     
             ); 
             
                     return $result;
    }
    

    public function insertarPersona(Persona $persona)
    {
         //dd($persona);
         $result=DB::select('
            
         select * from mgpersona_insertar(
                :pidpais,
                :pidubigeo,
                :pidtipopersona,
                :pidsexo,
                :papellidopaterno,
                :papellidomaterno,
                :pnombres,
                :pnombrecompleto,
                :pfechanacimiento,
                :pcorreoelectronicoempresarial,
                :pdireccion,
                :pnumerodocumento,
                :ptelefono,
                :pestado,
                :paltaaplicacion,
                :pusuario
             )' ,
             [
  

            "idpais"=>$persona->getIdpais(),
            "idubigeo"=>$persona->getIdubigeo(),
            "idtipopersona"=>$persona->getIdtipopersona(),
            "idsexo"=>$persona->getIdsexo(),
            "apellidopaterno"=>$persona->getApellidopaterno(),
            "apellidomaterno"=>$persona->getApellidomaterno(),
            "nombres"=>$persona->getNombres(),
            "nombrecompleto"=>$persona->getNombrecompleto(),
            "fechanacimiento"=>$persona->getFechanacimiento(),
            "correoelectronicoempresarial"=>$persona->getCorreoelectronicoempresarial(),
            "direccion"=>$persona->getDireccion(),
            "numerodocumento"=>$persona->getNumerodocumento(),
            "telefono"=>$persona->getTelefono(),
            "estado"=>$persona->getEstado(),
            "altaaplicacion"=>$persona->getAltaaplicacion(),
            "usuario"=>$persona->getUsuario(),
             ]
     
             ); 
             
                     return $result;
    }
    
}
?>