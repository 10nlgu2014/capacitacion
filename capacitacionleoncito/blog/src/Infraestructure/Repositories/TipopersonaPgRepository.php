<?php
namespace Infraestructure\Repositories;
use Domain\Tipopersona\tipopersonaRepositoryInterface;
use Domain\Tipopersona\tipopersona as Tipopersona;
use Illuminate\Support\Facades\DB;


final class TipopersonaPgRepository implements tipopersonaRepositoryInterface
{
    public function listarTipopersona(Tipopersona $tipopersona)
    {
        
        $result=DB::select('
            
        select * from mgtipopersona_listar(
                :pid,
                :pcodigo,
                :pdescripcion,
                :pestado,
                :pusuario

            )' ,

            [

                "pid"=>$tipopersona->getId(),
                "pcodigo"=>$tipopersona->getCodigo(),
                "pdescripcion"=>$tipopersona->getDescripcion(),
                "pestado"=>$tipopersona->getEstado(),
                "pusuario"=>$tipopersona->getUsuario(),

            ]
            );
            
                    return $result;
        }

    public function cargarTipopersona(Tipopersona $tipopersona)
    {
        $result =DB::select(
                'select* from mgtipopersona_cargar(:pusuario)',
                [
                    "pusuario"=>$tipopersona->getUsuario()
                ]
        );
        
            return $result;
    }

    public function modificarTipopersona(Tipopersona $tipopersona)
    {
         //dd($pais);
         $result=DB::select('
            
         select * from mgtipopersona_modificar(

                :pid,
                :pdescripcion,
                :pusuario
             )' ,
             [


                "pid"=>$tipopersona->getId(),
                "pdescripcion"=>$tipopersona->getDescripcion(),
                "pusuario"=>$tipopersona->getUsuario(),
             ]
     
             ); 
             
                     return $result;
    }
    

    public function insertartipopersona(Tipopersona $tipopersona)
    {
         //dd($persona);
         $result=DB::select('
            
         select * from mgtipopersona_insertar(
                    :pcodigo,
                    :pdescripcion,
                    :pestado,
                    :paltaaplicacion,
                    :pusuario
             )' ,
             [
 
            "pcodigo"=>$tipopersona->getCodigo(),
            "pdescripcion"=>$tipopersona->getDescripcion(),
            "pestado"=>$tipopersona->getEstado(),
            "paltaaplicacion"=>$tipopersona->getAltaaplicacion(),
            "pusuario"=>$tipopersona->getUsuario(),
             ]
     
             ); 
             
                     return $result;
    }
    
}
?>