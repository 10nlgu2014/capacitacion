<?php
    namespace Infraestructure\Repositories;
    use Domain\Funcion\funcionRepositoryInterface;
    use Domain\Funcion\funcion as Funcion;
    use Illuminate\Support\Facades\DB;

final class FuncionPgRepository implements funcionRepositoryInterface
{
        public function listarFuncion(Funcion $funcion){
            $result=DB::select(
                'select* from sefuncion_listar(
                    :pid,
                    :pcodigo,
                    :pdescripcion,
                    :pcomentario,
                    :pestado,
                    :paltaaplicacion,
                    :pusuario
                )',
                [
                    "pid"=>$funcion->getId(),
                    "pcodigo"=>$funcion->getCodigo(),
                    "pdescripcion"=>$funcion->getDescripcion(),
                    "pcomentario"=>$funcion->getComentario(),
                    "pestado"=>$funcion->getEstado(),
                    "paltaaplicacion"=>$funcion->getAltaaplicacion(),
                    "pusuario"=>$funcion->getUsuario(),

                ]
            );
            return $result;
        }

        public function cargarFuncion(Funcion $funcion)
        {
            $result=DB::select('
                select * from sefuncion_cargar
                (
                   :pusuario
                )',
                [
                "pusuario"=>$funcion->getUsuario(),
                ]);
                return $result;
        }

        public function modificarFuncion(Funcion $funcion){

            $result=DB::select('
                select * from sefuncion_modificar(
                    :pid,
                    :pcodigo,
                    :pdescripcion,
                    :pcomentario,
                    :pestado,
                    :paltaaplicacion,
                    :pusuario               
                 )',
                 [
                    "pid"=>$funcion->getId(),
                    "pcodigo"=>$funcion->getCodigo(),
                    "pdescripcion"=>$funcion->getDescripcion(),
                    "pcomentario"=>$funcion->getComentario(),
                    "pestado"=>$funcion->getEstado(),
                    "paltaaplicacion"=>$funcion->getAltaaplicacion(),
                    "pusuario"=>$funcion->getUsuario(),

                 ]);
                 return $result;
        }

        public function insertarFuncion(Funcion $funcion){

            $result=DB::select('
                select * from sefuncion_insertar
                (
                    
                    :pcodigo,
                    :pdescripcion,
                    :pcomentario,
                    :pestado,
                    :paltaaplicacion,
                    :pusuario

                )',
                [   
                    "pcodigo"=>$funcion->getCodigo(),
                    "pdescripcion"=>$funcion->getDescripcion(),
                    "pcomentario"=>$funcion->getComentario(),
                    "pestado"=>$funcion->getEstado(),
                    "paltaaplicacion"=>$funcion->getaltaAplicacion(),
                    "pusuario"=>$funcion->getUsuario(),
                    
                ]);
                return $result;
        }
}

?>