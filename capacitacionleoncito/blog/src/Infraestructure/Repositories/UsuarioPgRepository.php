<?php
namespace Infraestructure\Repositories;
use Domain\Usuario\usuarioRepositoryInterface;
use Domain\Usuario\usuario as Usuario;
use Illuminate\Support\Facades\DB;


final class UsuarioPgRepository implements usuarioRepositoryInterface
{
    public function listarUsuario(Usuario $usuario)
    {
        
        $result=DB::select('
            
        select * from seusuario_listar(
                :pid,
                :pidpersona,
                :pnombre,
                :pclave,
                :pexpiraclaveflag,
                :pfechaexpiracion,
                :pultimologin,
                :pestado,
                :paltaaplicacion,
                :pusuario
            )' ,
            [

                ":pid"=>$usuario->getId(),
                ":pidpersona"=>$usuario->getIdpersona(),
                ":pnombre"=>$usuario->getNombre(),
                ":pclave"=>$usuario->getClave(),
                ":pexpiraclaveflag"=>$usuario->getExpiraclaveflag(),
                ":pfechaexpiracion"=>$usuario->getFechaexpiracion(),
                ":pultimologin"=>$usuario->getUltimologin(),
                ":pestado"=>$usuario->getEstado(),
                ":paltaaplicacion"=>$usuario->getAltaaplicacion(),
                ":pusuario"=>$usuario->getUsuario(),
            ]
    
            );
            
                    return $result;
    }

    public function cargarUsuario(Usuario $usuario)
    {
        $result =DB::select(
                'select* from seusuario_cargar(:pusuario)',
                [
                    ":pusuario"=>$usuario->getUsuario()
                ]
        );
        
            return $result;
    }

    public function modificarUsuario(Usuario $usuario)
    {
         //dd($pais);
         $result=DB::select('
            
         select * from seusuario_modificar(

            :pidpersona,
            :pnombre,
            :pclave,
            :pexpiraclaveflag,
            :pfechaexpiracion,
            :pultimologin,
            :pestado,
            :paltaaplicacion,
            :pusuario
             )' ,
             [

                "pidpersona"=>$usuario->getIdpersona(),
                "pnombre"=>$usuario->getNombre(),
                "pclave"=>$usuario->getClave(),
                "pexpiraclaveflag"=>$usuario->getExpiraclaveflag(),
                "pfechaexpiracion"=>$usuario->getFechaexpiracion(),
                "pultimologin"=>$usuario->getUltimologin(),
                "pestado"=>$usuario->getEstado(),
                "paltaaplicacion"=>$usuario->getAltaaplicacion(),
                "pusuario"=>$usuario->getUsuario(),


             ]
     
             ); 
             
                     return $result;
    }
    

    public function insertarUsuario(Usuario $usuario)
    {
         //dd($pais);
         $result=DB::select('
            
         select * from seusuario_insertar( 
            :pidpersona,
            :pnombre,
            :pclave,
            :pexpiraclaveflag,
            :pfechaexpiracion,
            :pultimologin,
            :pestado,
            :paltaaplicacion,
            :pusuario
             )' ,
             [
                
                "pidpersona"=>$usuario->getIdpersona(),
                "pnombre"=>$usuario->getNombre(),
                "pclave"=>$usuario->getClave(),
                "pexpiraclaveflag"=>$usuario->getExpiraclaveflag(),
                "pfechaexpiracion"=>$usuario->getFechaexpiracion(),
                "pultimologin"=>$usuario->getUltimologin(),
                "pestado"=>$usuario->getEstado(),
                "paltaaplicacion"=>$usuario->getAltaaplicacion(),
                "pusuario"=>$usuario->getUsuario(),
             ]
     
             ); 
             
                     return $result;
    }
    
}
?>