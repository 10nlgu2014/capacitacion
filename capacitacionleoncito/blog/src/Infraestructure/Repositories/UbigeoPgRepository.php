<?php
namespace Infraestructure\Repositories;
use Domain\Ubigeo\ubigeoRepositoryInterface;
use Domain\Ubigeo\ubigeo as Ubigeo;
use Illuminate\Support\Facades\DB;


final class UbigeoPgRepository implements ubigeoRepositoryInterface
{
    public function listarUbigeo(Ubigeo $ubigeo)
    {
        //dd($pais);
        $result=DB::select('
            
        select * from mgubigeo_listar(

                :pid,
                :pidpais,
                :pidubigeo,
                :pcodigo,
                :pnivel,
                :pcodigoiso,
                :pdescripcioncorta,
                :pdescripcionlarga,
                :ptienedetalle,
                :pestado,
                :paltaaplicacion,
                :pusuario
            )', 
            [

            "pid"=>$ubigeo->getId(),
            "pidpais"=>$ubigeo->getIdpais(),
            "pidubigeo"=>$ubigeo->getIdubigeo(),
            "pcodigo"=>$ubigeo->getCodigo(),
            "pnivel"=>$ubigeo->getNivel(),
            "pcodigoiso"=>$ubigeo->getCodigoiso(),
            "pdescripcioncorta"=>$ubigeo->getDescripcioncorta(),
            "pdescripcionlarga"=>$ubigeo->getDescripcionlarga(),
            "ptienedetalle"=>$ubigeo->getTienedetalle(),
            "pestado"=>$ubigeo->getEstado(),
            "paltaaplicacion"=>$ubigeo->getAltaaplicacion(),
            "pusuario"=>$ubigeo->getUsuario(),
			

            ]
    
            );
            
                    return $result;
    }

    public function cargarUbigeo(Ubigeo $ubigeo)
    {
        $result =DB::select(
                'select* from mgubigeo_cargar(:pusuario)',
                [
                    "pusuario"=>$ubigeo->getUsuario()
                ]
        );
        
            return $result;
    }

    public function modificarUbigeo(Ubigeo $ubigeo)
    {
         //dd($pais);
         $result=DB::select('
            
         select * from mgubigeo_modificar(

            :pidpais,
            :pidubigeo,
            :pcodigo,
            :pnivel,
            :pcodigoiso,
            :pdescripcioncorta,
            :pdescripcionlarga,
            :ptienedetalle,
            :pestado,
            :paltaaplicacion,
            :pusuario
             )' ,
             [

            "pidpais"=>$ubigeo->getIdpais(),
            "pidubigeo"=>$ubigeo->getIdubigeo(),
            "pcodigo"=>$ubigeo->getCodigo(),
            "pnivel"=>$ubigeo->getNivel(),
            "pcodigoiso"=>$ubigeo->getCodigoiso(),
            "pdescripcioncorta"=>$ubigeo->getDescripcioncorta(),
            "pdescripcionlarga"=>$ubigeo->getDescripcionlarga(),
            "ptienedetalle"=>$ubigeo->getTienedetalle(),
            "pestado"=>$ubigeo->getestado(),
            "paltaaplicacion"=>$ubigeo->getAltaaplicacion(),
            "pusuario"=>$ubigeo->getUsuario(),

             ]
     
             ); 
             
                     return $result;
    }
    

    public function insertarUbigeo(Ubigeo $ubigeo)
    {
         //dd($pais);
         $result=DB::select('
            
         select * from mgubigeo_insertar(

            :pidpais,
            :pidubigeo,
            :pcodigo,
            :pnivel,
            :pcodigoiso,
            :pdescripcioncorta,
            :pdescripcionlarga,
            :ptienedetalle,
            :pestado,
            :paltaaplicacion,
            :pusuario
             )' ,
             [

                "pidpais"=>$ubigeo->getIdpais(),
                "pidubigeo"=>$ubigeo->getIdubigeo(),
                "pcodigo"=>$ubigeo->getcodigo(),
                "pnivel"=>$ubigeo->getNivel(),
                "pcodigoiso"=>$ubigeo->getCodigoiso(),
                "pdescripcioncorta"=>$ubigeo->getDescripcioncorta(),
                "pdescripcionlarga"=>$ubigeo->getDescripcionlarga(),
                "ptienedetalle"=>$ubigeo->getTienedetalle(),
                "pestado"=>$ubigeo->getEstado(),
                "paltaaplicacion"=>$ubigeo->getAltaaplicacion(),
                "pusuario"=>$ubigeo->getUsuario(),
             ]
     
             ); 
             
                     return $result;
    }
    
}
?>