<?php
namespace Infraestructure\Repositories;
use Domain\Pais\paisRepositoryInterface;
use Domain\Pais\pais as Pais;
use Illuminate\Support\Facades\DB;


final class PaisPgRepository implements paisRepositoryInterface
{
    public function listarPais(Pais $pais)
    {
        
        $result=DB::select('
            
        select * from mgpais_listar(
                :pid,
                :pcodigo,
                :pdescripcioncorta,
                :pdescripcionlarga,
                :pnombreiso,
                :pnacionalidad,
                :pprefijotelefonico,
                :pformatotelefonico,
                :pnombredocumentoidentidad,
                :psigladocumentoidentidad,
                :plongituddocumentoidentidad,
                :pestado,
                :pusuario
            )' ,
            [
                "pid"=>$pais->getId(),
                "pcodigo"=>$pais->getCodigo(),
                "pdescripcioncorta"=>$pais->getDescripcioncorta(),
                "pdescripcionlarga"=>$pais->getDescripcionlarga(),
                "pnombreiso"=>$pais->getNombreiso(),
                "pnacionalidad"=>$pais->getNacionalidad(),
                "pprefijotelefonico"=>$pais->getPrefijotelefonico(),
                "pformatotelefonico"=>$pais->getFormatotelefonico(),
                "pnombredocumentoidentidad"=>$pais->getNombredocumentoidentidad(),
                "psigladocumentoidentidad"=>$pais->getSigladocumentoidentidad(),
                "plongituddocumentoidentidad"=>$pais->getLongituddocumentoidentidad(),
                "pestado"=>$pais->getEstado(),
                "pusuario"=>$pais->getUsuario(),
            ]
    
            );
            
                    return $result;
    }

    public function cargarPais(Pais $pais)
    {
        $result =DB::select(
                'select* from mgpais_cargar(:pusuario)',
                [
                    "pusuario"=>$pais->getUsuario()
                ]
        );
        
            return $result;
    }

    public function modificarPais(Pais $pais)
    {
         //dd($pais);
         $result=DB::select('
            
         select * from mgpais_modificar(
                 :pid,
                 :pdescripcioncorta,
                 :pdescripcionlarga,
                 :pnombreiso,
                 :pnacionalidad,
                 :pprefijotelefonico,
                 :pformatotelefonico,
                 :pnombredocumentoidentidad,
                 :psigladocumentoidentidad,
                 :plongituddocumentoidentidad,
                 :pestado,
                 :pusuario
             )' ,
             [
                
                 "pid"=>$pais->getId(),
                 "pdescripcioncorta"=>$pais->getDescripcioncorta(),
                 "pdescripcionlarga"=>$pais->getDescripcionlarga(),
                 "pnombreiso"=>$pais->getNombreiso(),
                 "pnacionalidad"=>$pais->getNacionalidad(),
                 "pprefijotelefonico"=>$pais->getPrefijotelefonico(),
                 "pformatotelefonico"=>$pais->getFormatotelefonico(),
                 "pnombredocumentoidentidad"=>$pais->getNombredocumentoidentidad(),
                 "psigladocumentoidentidad"=>$pais->getSigladocumentoidentidad(),
                 "plongituddocumentoidentidad"=>$pais->getLongituddocumentoidentidad(),
                 "pestado"=>$pais->getEstado(),
                 "pusuario"=>$pais->getUsuario(),
             ]
     
             ); 
             
                     return $result;
    }
    

    public function insertarPais(Pais $pais)
    {
         //dd($pais);
         $result=DB::select('
            
         select * from mgpais_insertar(
                 :pcodigo,
                 :pdescripcioncorta,
                 :pdescripcionlarga,
                 :pnombreiso,
                 :pnacionalidad,
                 :pprefijotelefonico,
                 :pformatotelefonico,
                 :pnombredocumentoidentidad,
                 :psigladocumentoidentidad,
                 :plongituddocumentoidentidad,
                 :pestado,
                 :paltaaplicacion,
                 :pusuario
             )' ,
             [
                
                 "pcodigo"=>$pais->getCodigo(),
                 "pdescripcioncorta"=>$pais->getDescripcioncorta(),
                 "pdescripcionlarga"=>$pais->getDescripcionlarga(),
                 "pnombreiso"=>$pais->getNombreiso(),
                 "pnacionalidad"=>$pais->getNacionalidad(),
                 "pprefijotelefonico"=>$pais->getPrefijotelefonico(),
                 "pformatotelefonico"=>$pais->getFormatotelefonico(),
                 "pnombredocumentoidentidad"=>$pais->getNombredocumentoidentidad(),
                 "psigladocumentoidentidad"=>$pais->getSigladocumentoidentidad(),
                 "plongituddocumentoidentidad"=>$pais->getLongituddocumentoidentidad(),
                 "pestado"=>$pais->getEstado(),
                 "paltaaplicacion"=>$pais->getAltaaplicacion(),
                 "pusuario"=>$pais->getUsuario(),
             ]
     
             ); 
             
                     return $result;
    }
    
}
?>