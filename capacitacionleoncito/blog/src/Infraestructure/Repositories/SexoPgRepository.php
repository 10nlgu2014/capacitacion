<?php
namespace Infraestructure\Repositories;
use Domain\Sexo\sexoRepositoryInterface;
use Domain\Sexo\sexo as Sexo;
use Illuminate\Support\Facades\DB;

final class SexoPgRepository implements sexoRepositoryInterface
{
    public function listarSexo(Sexo $sexo)
    {
        $result=DB::select('
            select * from mgsexo_listar(
                :pid,
                :pcodigo,
                :pdescripcion,
                :pestado,
                :pusuario
            )' ,  
            [
                "pid"=>$sexo->getId(),
                "pcodigo"=>$sexo->getCodigo(),
                "pdescripcion"=>$sexo->getDescripcion(),
                "pestado"=>$sexo->getEstado(),
                "pusuario"=>$sexo->getUsuario(),
            ]
    
            );
                    return $result;
    }

    public function cargarSexo(Sexo $sexo)
    {
        $result =DB::select(
                'select * from mgsexo_cargar(:pusuario)',
                [
                    "usuario"=>$sexo->getUsuario()
                ]
        );
            return $result;
    }

    public function modificarSexo(Sexo $sexo)
    {
        $result=DB::select('
            select * from mgsexo_modificar(
                :pid,
                :pcodigo,
                :pdescripcion,
                :pestado,
                :paltaaplicacion,
                :pusuario
            )' ,  
            [
                "pid"=>$sexo->getId(),
                "pcodigo"=>$sexo->getCodigo(),
                "pdescripcion"=>$sexo->getDescripcion(),
                "pestado"=>$sexo->getEstado(),
                "paltaaplicacion"=>$sexo->getAltaaplicacion(),
                "pusuario"=>$sexo->getUsuario(),
            ]
        
    
            );
                    return $result;
    }
    public function insertarsexo(Sexo $sexo)
    {
        $result=DB::select('
            select * from mgsexo_insertar(
                
                :pcodigo,
                :pdescripcion,
                :pestado,
                :paltaaplicacion,
                :pusuario
            )' ,  
            [
                
                "pcodigo"=>$sexo->getCodigo(),
                "pdescripcion"=>$sexo->getDescripcion(),
                "pestado"=>$sexo->getEstado(),
                "paltaaplicacion"=>$sexo->getAltaaplicacion(),
                "pusuario"=>$sexo->getUsuario(),
            ]
        
    
            );
                    return $result;
    }
}
?>


